package uk.org.iwm.kiosk.flexikiosk.loader 
{
	import com.afriendcalledben.media.subtitles.SubtitlesManager;
	import com.afriendcalledben.style.FontLoader;
	import com.afriendcalledben.style.StyleManager;
	import com.afriendcalledben.text.TextManager;
	import com.afriendcalledben.media.MediaManager;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.XMLLoader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ApplicationLoader extends EventDispatcher
	{
		private var _loadError:Boolean = false
		private var _fontLoader:FontLoader = new FontLoader();
		
		public function ApplicationLoader() 
		{
			
		}
		
		public function load(contentDirectory:File):void
		{
			// Load XML
			var xmlQueue:LoaderMax = new LoaderMax( { onComplete:onXMLLoaded } );
			xmlQueue.append(new XMLLoader(contentDirectory.resolvePath(ApplicationVO.TEXT_XML).url, { name:ApplicationVO.TEXT_XML, onError:onXMLError } ) );
			xmlQueue.append(new XMLLoader(contentDirectory.resolvePath(ApplicationVO.MEDIA_XML).url, { name:ApplicationVO.MEDIA_XML, onError:onXMLError } ) );
			xmlQueue.append(new XMLLoader(contentDirectory.resolvePath(ApplicationVO.STRUCTURE_XML).url, { name:ApplicationVO.STRUCTURE_XML, onError:onXMLError } ) );
			xmlQueue.load();
		}
		
		private function onXMLLoaded(e:LoaderEvent):void
		{
			try {
				var textXML:XML = XML(LoaderMax.getContent(ApplicationVO.TEXT_XML));
			} catch (e:Error) {
				Log.log('The xml/text.xml file is incorrectly formatted. To identify the issue, open the XML file in a browser like Chrome', LogEvent.ERROR);
				_loadError = true;
			}
			
			try {
				var mediaXML:XML = XML(LoaderMax.getContent(ApplicationVO.MEDIA_XML));
			} catch (e:Error) {
				Log.log('ERROR: The xml/media.xml file is incorrectly formatted. To identify the issue, open the XML file in a browser like Chrome', LogEvent.ERROR);
				_loadError = true;
			}
			
			try {
				var structureXML:XML = XML(LoaderMax.getContent(ApplicationVO.STRUCTURE_XML));
			} catch(e:Error) {
				Log.log('ERROR: The xml/structure.xml file is incorrectly formatted. To identify the issue, open the XML file in a browser like Chrome', LogEvent.ERROR);
				_loadError = true;
			}
			if (_loadError) return;
			
			TextManager.setXML(textXML);
			
			Log.log('All XML files correct and loaded.');
			
			ApplicationVO.structureXML = structureXML;
			ApplicationManager.setXML(structureXML);
			
			var bitmapList:XMLList = mediaXML..media.(attribute('type') == 'image');
			var audioList:XMLList = mediaXML..media.(attribute('type') == 'audio');
			var videoList:XMLList = mediaXML..media.(attribute('type') == 'video');
			var subtitlesList:XMLList = mediaXML..media.(attribute('type') == 'srt');
			var swfList:XMLList = mediaXML..media.(attribute('type') == 'swf');
			
			MediaManager.addEventListener(MediaManager.BITMAPS_LOADED, onLoaded);
			MediaManager.addEventListener(MediaManager.AUDIO_LOADED, onLoaded);
			MediaManager.addEventListener(MediaManager.SWFS_LOADED, onLoaded);
			
			MediaManager.setBitmaps(bitmapList, ApplicationVO.contentDirectoryFile);
			MediaManager.setAudios(audioList, ApplicationVO.contentDirectoryFile);
			MediaManager.setVideos(videoList, ApplicationVO.contentDirectoryFile);
			MediaManager.setSubtitles(subtitlesList, ApplicationVO.contentDirectoryFile);
			MediaManager.setSwfs(swfList, ApplicationVO.contentDirectoryFile);
			
			_fontLoader.addEventListener(Event.COMPLETE, onLoaded);
			_fontLoader.load([ApplicationVO.contentDirectoryFile.resolvePath(ApplicationVO.FONT_SWF).url]);
			
			StyleManager.addEventListener(Event.COMPLETE, onLoaded);
			StyleManager.loadStyleSheet(ApplicationVO.contentDirectoryFile.resolvePath(ApplicationVO.STYLES_CSS).url);
		}
		
		private function onXMLError(e:LoaderEvent):void
		{
			_loadError = true;
			switch (XMLLoader(e.target).name)
			{
				case ApplicationVO.TEXT_XML :
				Log.log('xml/text.xml not found in the content directory.', LogEvent.ERROR);
				break;
				case ApplicationVO.MEDIA_XML :
				Log.log('xml/media.xml not found in the content directory.', LogEvent.ERROR);
				break;
				case ApplicationVO.STRUCTURE_XML :
				Log.log('xml/structure.xml not found in the content directory.', LogEvent.ERROR);
				break;
			}
		}
		
		private function onLoaded(e:Event):void
		{
			if (MediaManager.bitmapsLoaded && MediaManager.audioLoaded && MediaManager.swfsLoaded && _fontLoader.loaded && StyleManager.loaded) {
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
	}

}