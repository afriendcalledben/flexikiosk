package uk.org.iwm.kiosk.flexikiosk.vo 
{
	import flash.filesystem.File;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ApplicationVO 
	{
		private static var _applicationWidth:int;
		private static var _applicationHeight:int;
		private static var _contentDirectory:String = '';
		private static var _contentDirectoryFile:File;
		private static var _structureXML:XML;
		
		public static const FONT_SWF:String = 'css/fonts.swf';
		public static const STYLES_CSS:String = 'css/styles.css';
		public static const TEXT_XML:String = 'xml/text.xml';
		public static const MEDIA_XML:String = 'xml/media.xml';
		public static const STRUCTURE_XML:String = 'xml/structure.xml';
		public static const OVERLAY_TRANSPARENCY:Number = 0.8;
		
		public static function get applicationWidth():int
		{
			return _applicationWidth;
		}
		
		public static function set applicationWidth(value:int):void
		{
			_applicationWidth = value;
		}
		
		public static function get applicationHeight():int
		{
			return _applicationHeight;
		}
		
		public static function set applicationHeight(value:int):void
		{
			_applicationHeight = value;
		}
		
		public static function get contentDirectory():String
		{
			return _contentDirectory;
		}
		
		public static function set contentDirectory(value:String):void
		{
			_contentDirectory = value;
		}
		
		public static function get contentDirectoryFile():File
		{
			return _contentDirectoryFile;
		}
		
		public static function set contentDirectoryFile(value:File):void
		{
			_contentDirectoryFile = value;
		}
		
		public static function get structureXML():XML
		{
			return _structureXML;
		}
		
		public static function set structureXML(value:XML):void
		{
			_structureXML = value;
		}
		
	}

}