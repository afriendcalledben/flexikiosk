package uk.org.iwm.kiosk.flexikiosk.log 
{
	import flash.events.Event;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class LogEvent extends Event
	{
		public static const LOG:String = 'log';
		
		public static const INFO:int = 1;
		public static const WARNING:int = 3;
		public static const ERROR:int = 5;
		
		private var _logType:int;
		private var _message:String;
		private var _time:Date;
		
		public function LogEvent(message:String, logtype:int = INFO) 
		{
			super(LOG, bubbles, cancelable);
			
			_logType = logtype;
			_message = message;
			_time = new Date();
		}
		
		public function get logType():int
		{
			return _logType;
		}
		
		public function get message():String
		{
			return _message;
		}
		
		public function get time():Date
		{
			return _time;
		}
		
	}

}