package uk.org.iwm.kiosk.flexikiosk.log 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import com.afriendcalledben.services.DateFormatter;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class LogConsole extends Sprite
	{
		private var _outputTF:TextField;
		
		private var GENERAL_COLOR:String = "FFFFFF";
		private var WARNING_COLOR:String = "CC9900";
		private var ERROR_COLOR:String = "FF0000";
		
		public function LogConsole(w:int, h:int, gc:String = "FFFFFF", wc:String = "CC9900", ec:String = "FF0000") 
		{
			GENERAL_COLOR = gc;
			WARNING_COLOR = wc;
			ERROR_COLOR = ec;
			
			graphics.lineStyle(1, uint('0x'+GENERAL_COLOR));
			graphics.drawRect(0, 0, w, h);
			
			var tfo:TextFormat = new TextFormat('Courier', 14, uint('0x'+GENERAL_COLOR));
			tfo.leading = 5;
			
			_outputTF = new TextField();
			_outputTF.width = w - 20;
			_outputTF.height = h - 20;
			_outputTF.wordWrap = _outputTF.multiline = true;
			_outputTF.defaultTextFormat = tfo;
			_outputTF.x = _outputTF.y = 10;
			addChild(_outputTF);
			
			Log.addEventListener(LogEvent.LOG, addLogEvent);
		}
		
		public function addLogEvent(evt:LogEvent):void
		{
			var fMessage:String;
			if (evt.logType == LogEvent.WARNING) {
				fMessage = '<b>[' + formatTime(evt.time) + '] <font color="#'+WARNING_COLOR+'">WARNING:</font></b> ' + evt.message;
			} else if (evt.logType == LogEvent.ERROR) {
				fMessage = '<b>[' + formatTime(evt.time) + '] <font color="#'+ERROR_COLOR+'">ERROR:</font></b> ' + evt.message;
			} else {
				fMessage = '<b>[' + formatTime(evt.time) + ']</b> ' + evt.message;
			}
			addOutputText(fMessage);
		}
		
		private function addOutputText(op:String):void
		{
			_outputTF.htmlText = op + "<br />" + _outputTF.htmlText;
		}
		
		public function set totalLog(value:String):void
		{
			_outputTF.htmlText = value;
		}
		
		public function get totalLog():String
		{
			return _outputTF.htmlText;
		}
		
		private function formatTime(time:Date):String
		{
			var dateStr:String = DateFormatter.getDateString(time, '/');
			var timeStr:String = DateFormatter.getTimeString(time, ':');
			
			return (dateStr + ' ' + timeStr);
		}
		
	}

}