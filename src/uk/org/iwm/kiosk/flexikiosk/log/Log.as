package uk.org.iwm.kiosk.flexikiosk.log 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class Log 
	{
		protected static var disp:EventDispatcher;
		private static var _logSender:LogSender;
		
		public static function log(value:*, type:int = LogEvent.INFO):void
		{
			trace(value);
			var evt:LogEvent = new LogEvent(String(value), type);
			dispatchEvent(evt);
		}
		
		public static function addEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false, p_priority:int=0, p_useWeakReference:Boolean=false):void {
			if (disp == null) { disp = new EventDispatcher(); }
			disp.addEventListener(p_type, p_listener, p_useCapture, p_priority, p_useWeakReference);
		}
		
		public static function removeEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false):void {
			if (disp == null) { return; }
			disp.removeEventListener(p_type, p_listener, p_useCapture);
		}
		
		public static function dispatchEvent(p_event:Event):void {
			if (disp == null) { return; }
			disp.dispatchEvent(p_event);
		}
	}

}