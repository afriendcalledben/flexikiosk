package uk.org.iwm.kiosk.flexikiosk.log
{ 
	import flash.events.StatusEvent;
    import flash.net.LocalConnection; 
	import flash.system.Security;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	
    public class LogSender
    { 
		private var _localConnection:LocalConnection = new LocalConnection();
		private var _connectionName:String;
		
        public function LogSender(connectionName:String) 
        { 
			_connectionName = connectionName;
			_localConnection.addEventListener(StatusEvent.STATUS, onStatus);
			
            try 
            { 
				_localConnection.allowDomain('*');
                _localConnection.connect(connectionName);
				trace('Connected');
            } 
            catch (error:ArgumentError) 
            { 
                trace('Could not connect');
            } 
        }
		
        public function sendLogEvent(evt:LogEvent):void 
        {
			_localConnection.send('app#uk.org.iwm.kiosk.flexikiosk.logger.IWMFlexiKioskLogger:'+_connectionName, 'recieveLogEvent', evt.message, evt.logType, evt.time);
        } 
        
        private function onStatus(event:StatusEvent):void {
            switch (event.level) {
                case "status":
                    trace("LocalConnection.send() succeeded");
                    break;
                case "error":
                    trace("LocalConnection.send() failed");
                    break;
            }
        }
    } 
}