package uk.org.iwm.kiosk.flexikiosk.model 
{
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class NavigationBarTitleVO 
	{
		private var _id:String;
		private var _position:String;
		
		public function NavigationBarTitleVO(id:String, position:String) 
		{
			_id = id;
			_position = position;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get position():String
		{
			return _position;
		}
		
	}

}