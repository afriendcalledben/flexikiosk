package uk.org.iwm.kiosk.flexikiosk.model 
{
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class GalleryVO 
	{
		private var _slides:Vector.<GallerySlideVO>;
		private var _slideOverlap:int;
		
		public function GalleryVO() 
		{
			
		}
		
		public function processXML(xml:XML):void
		{
			_slideOverlap = (xml..slideOverlap.length() > 0) ? xml..slideOverlap[0] : 200;
			
			_slides = new Vector.<GallerySlideVO>();
			
			for (var i:int = 0; i < xml..slide.length(); i++)
			{
				var txt:String = (xml..slide[i].text.length() > 0) ? xml..slide[i].text[0] : '';
				var image:String = (xml..slide[i].image.length() > 0) ? xml..slide[i].image[0] : '';
				var audio:String = (xml..slide[i].audio.length() > 0) ? xml..slide[i].audio[0] : '';
				var video:String = (xml..slide[i].video.length() > 0) ? xml..slide[i].video[0] : '';
				var accessibility:String = (xml..slide[i].accessibility.length() > 0) ? xml..slide[i].accessibility[0] : '';
				var click_task:String = (xml..slide[i].click_task.length() > 0) ? xml..slide[i].click_task[0] : '';
				var show_task:String = (xml..slide[i].show_task.length() > 0) ? xml..slide[i].show_task[0] : '';
				var gsvo:GallerySlideVO = new GallerySlideVO(txt, image, audio, video, accessibility, click_task, show_task);
				_slides.push(gsvo);
			}
		}
		
		public function get slideOverlap():int 
		{
			return _slideOverlap;
		}
		
		public function getSlides():Vector.<GallerySlideVO>
		{
			return _slides;
		}
		
	}

}