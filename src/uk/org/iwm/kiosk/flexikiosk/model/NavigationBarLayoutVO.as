package uk.org.iwm.kiosk.flexikiosk.model 
{
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class NavigationBarLayoutVO 
	{
		private var _hidden:Boolean;
		private var _buttons:Vector.<NavigationBarButtonVO>;
		private var _title:NavigationBarTitleVO;
		private var _clicktask:String;
		
		public function NavigationBarLayoutVO() 
		{
			
		}
		
		public function processXML(xml:XML):void
		{
			_hidden = (xml..hidden[0] == 'true') ? true : false;
			
			_buttons = new Vector.<NavigationBarButtonVO>();
			
			for (var i:int = 0; i < xml..button.length(); i++) {
				var position:String = xml..button[i].position[0];
				var image:String = (xml..button[i].image.length() > 0) ? xml..button[i].image[0] : '';
				var click_task:String = (xml..button[i].click_task.length() > 0) ? xml..button[i].click_task[0] : '';
				var accessibility_task:String = (xml..button[i].accessibility_task.length() > 0) ? xml..button[i].accessibility_task[0] : '';
				var enabled:Boolean = (xml..button[i].enabled[0] != 'false');
				var bvo:NavigationBarButtonVO = new NavigationBarButtonVO(position, image, click_task, accessibility_task, enabled);
				_buttons.push(bvo);
			}
			
			if (xml..title.length() > 0) {
				var id:String = xml..title[0].text[0];
				position = String(xml..title[0].position[0]);
				_title = new NavigationBarTitleVO(id, position);
			}
			
			_clicktask = (xml.click_task.length() > 0) ? xml.click_task[0] : '';
		}
		
		public function get buttons():Vector.<NavigationBarButtonVO>
		{
			return _buttons;
		}
		
		public function get hidden():Boolean
		{
			return _hidden;
		}
		
		public function get title():NavigationBarTitleVO
		{
			return _title;
		}
		
		public function get clicktask():String
		{
			return _clicktask;
		}
		
	}

}