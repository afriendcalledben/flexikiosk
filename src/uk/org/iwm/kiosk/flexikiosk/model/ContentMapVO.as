package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import com.afriendcalledben.media.MediaManager;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ContentMapVO 
	{
		private var _id:String;
		private var _mapArea:Rectangle;
		private var _mapBackground:BackgroundVO;
		private var _hotspots:Vector.<HotspotVO>;
		
		public function ContentMapVO() 
		{
			
		}
		
		public function processXML(xml:XML):void
		{
			_id = xml.@id;
			var x:int = (xml..mapArea.x.length() > 0) ? xml..mapArea.x[0] : 0;
			var y:int = (xml..mapArea.y.length() > 0) ? xml..mapArea.y[0] : 0;
			var width:int = (xml..mapArea.width.length() > 0) ? xml..mapArea.width[0] : ApplicationVO.applicationWidth;
			var height:int = (xml..mapArea.height.length() > 0) ? xml..mapArea.height[0] : ApplicationVO.applicationHeight;
			
			_mapArea = new Rectangle(x, y, width, height);
			
			_mapBackground = new BackgroundVO();
			if (xml..mapArea.background.length() > 0) _mapBackground.processXML(xml..mapArea.background[0]);
			
			_hotspots = new Vector.<HotspotVO>();
			
			for (var i:int = 0; i < xml..hotspot.length(); i++) {
				var type:String;
				var mediaId:String;
				if (xml..hotspot[i].image.length() > 0) {
					type = HotspotVO.IMAGE;
					mediaId = xml..hotspot[i].image[0];
				} else if (xml..hotspot[i].swf.length() > 0) {
					type = HotspotVO.SWF;
					mediaId = xml..hotspot[i].swf[0];
				}
				var xPos:int = xml..hotspot[i].x[0];
				var yPos:int = xml..hotspot[i].y[0];
				var taskId:String = xml..hotspot[i].click_task[0];
				var hvo:HotspotVO = new HotspotVO(type, mediaId, xPos, yPos, taskId);
				_hotspots.push(hvo);
			}
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get mapArea():Rectangle
		{
			return _mapArea;
		}
		
		public function get mapBackground():BackgroundVO
		{
			return _mapBackground;
		}
		
		public function getHotspots():Vector.<HotspotVO>
		{
			return _hotspots;
		}
		
	}

}