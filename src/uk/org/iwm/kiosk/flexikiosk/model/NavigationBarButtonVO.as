package uk.org.iwm.kiosk.flexikiosk.model 
{
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class NavigationBarButtonVO 
	{
		private var _imageId:String;
		private var _position:String;
		private var _clickTask:String;
		private var _accessibilityTask:String;
		private var _enabled:Boolean;
		
		public function NavigationBarButtonVO(position:String, image:String = '', click_task:String = '', accessibility_task:String = '', enabled:Boolean = true) 
		{
			_imageId = image;
			_position = position;
			_clickTask = click_task;
			_accessibilityTask = accessibility_task;
			_enabled = enabled;
		}
		
		public function get image():String
		{
			return _imageId;
		}
		
		public function get position():String
		{
			return _position;
		}
		
		public function get clickTask():String
		{
			return _clickTask;
		}
		
		public function get accessibilityTask():String
		{
			return _accessibilityTask;
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
	}

}