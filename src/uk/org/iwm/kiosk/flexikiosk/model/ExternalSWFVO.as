package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import com.afriendcalledben.media.MediaManager;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ExternalSWFVO 
	{
		private var _id:String;
		private var _swf:String;
		private var _swfText:Array;
		private var _swfImages:Array;
		private var _tasks:Array;
		
		public function ExternalSWFVO() 
		{
			
		}
		
		public function processXML(xml:XML):void
		{
			_id = xml.@id;
			var x:int = (xml..mapArea.x.length() > 0) ? xml..mapArea.x[0] : 0;
			var y:int = (xml..mapArea.y.length() > 0) ? xml..mapArea.y[0] : 0;
			var width:int = (xml..mapArea.width.length() > 0) ? xml..mapArea.width[0] : ApplicationVO.applicationWidth;
			var height:int = (xml..mapArea.height.length() > 0) ? xml..mapArea.height[0] : ApplicationVO.applicationHeight;
			
			_swf = (xml..swf.length() > 0) ? String(xml..swf[0]) : '';
			
			var obj:Object;
			
			// SWF Text
			
			_swfText = [];
			var i:int;
			if (xml..swf_text.length() > 0)
			{
				for (i = 0; i < xml..swf_text[0].text.length(); i++)
				{
					obj = { };
					obj.id = xml..swf_text[0].text[i].@id[0];
					obj.text = xml..swf_text[0].text[i];
					_swfText.push(obj);
				}
			}
			
			// SWF Images
			
			_swfImages = [];
			if (xml..swf_images.length() > 0)
			{
				for (i = 0; i < xml..swf_images[0].image.length(); i++)
				{
					obj = { };
					obj.id = xml..swf_images[0].image[i].@id[0];
					obj.image = xml..swf_images[0].image[i];
					_swfImages.push(obj);
				}
			}
			
			// SWF Functions
			
			_tasks = [];
			
			for (i = 0; i < xml..swf_function.length(); i++)
			{
				obj = { };
				obj.event = xml..swf_function[i].event_type[0];
				obj.task = xml..swf_function[i].task[0];
				_tasks.push(obj);
			}
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get swf():String
		{
			return _swf;
		}
		
		public function getTasks():Array
		{
			return _tasks;
		}
		
		public function getText():Array
		{
			return _swfText;
		}
		
		public function getImages():Array
		{
			return _swfImages;
		}
		
	}

}