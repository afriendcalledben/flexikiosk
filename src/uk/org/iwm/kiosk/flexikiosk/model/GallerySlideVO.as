package uk.org.iwm.kiosk.flexikiosk.model 
{
	import com.afriendcalledben.media.MediaManager;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class GallerySlideVO 
	{
		private var _textId:String;
		private var _imageId:String;
		private var _audioId:String;
		private var _videoId:String;
		private var _accessibilityId:String;
		private var _clickTask:String;
		private var _showTask:String;
		
		public function GallerySlideVO(textId:String = '', imageId:String = '', audioId:String = '', videoId:String = '', accessibilityId:String = '', clickTask:String = '', showTask:String = '') 
		{
			_textId = textId;
			_imageId = imageId;
			_audioId = audioId;
			_videoId = videoId;
			_accessibilityId = accessibilityId;
			_clickTask = clickTask;
			_showTask = showTask;
		}
		
		public function get textId():String
		{
			return _textId;
		}
		
		public function get imageId():String
		{
			return _imageId;
		}
		
		public function get image():BitmapData
		{
			return MediaManager.getBitmapByID(_imageId);
		}
		
		public function get audioId():String
		{
			return _audioId;
		}
		
		public function get videoId():String
		{
			return _videoId;
		}
		
		public function get accessibilityId():String
		{
			return _accessibilityId;
		}
		
		public function get clickTask():String
		{
			return _clickTask;
		}
		
		public function get showTask():String
		{
			return _showTask;
		}
		
	}

}