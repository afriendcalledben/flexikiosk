package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.utils.Dictionary;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ActionVO 
	{
		// Types
		public static const NONE:String = 'NONE';
		public static const SET_CONTENT:String = 'SET_CONTENT';
		public static const SHOW_LAYER:String = 'SHOW_LAYER';
		public static const HIDE_LAYER:String = 'HIDE_LAYER';
		public static const SHOW_NAVIGATION_BAR:String = 'SHOW_NAVIGATION_BAR';
		public static const HIDE_NAVIGATION_BAR:String = 'HIDE_NAVIGATION_BAR';
		public static const SET_NAVIGATION_BAR_LAYOUT:String = 'SET_NAVIGATION_BAR_LAYOUT';
		public static const SET_NAVIGATION_BAR_TITLE:String = 'SET_NAVIGATION_BAR_TITLE';
		public static const SHOW_NAVIGATION_BAR_ALERT:String = 'SHOW_NAVIGATION_BAR_ALERT';
		public static const SHOW_FULLSCREEN_VIDEO:String = 'SHOW_FULLSCREEN_VIDEO';
		public static const SHOW_FULLSCREEN_IMAGE:String = 'SHOW_FULLSCREEN_IMAGE';
		public static const SHOW_FULLSCREEN_TEXT:String = 'SHOW_FULLSCREEN_TEXT';
		public static const SHOW_FULLSCREEN_AUDIO:String = 'SHOW_FULLSCREEN_AUDIO';
		public static const HIDE_FULLSCREEN:String = 'HIDE_FULLSCREEN';
		public static const START_TIMEOUT:String = 'START_TIMEOUT';
		public static const STOP_TIMEOUT:String = 'STOP_TIMEOUT';
		public static const PLAY_AUDIO:String = 'PLAY_AUDIO';
		public static const STOP_AUDIO:String = 'STOP_AUDIO';
		public static const GALLERY_NEXT:String = 'GALLERY_NEXT';
		public static const GALLERY_PREV:String = 'GALLERY_PREV';
		public static const SHOW_ACCESSIBILITY:String = 'SHOW_ACCESSIBILITY';
		
		private static const _typesArray:String = NONE + '**' + SET_CONTENT + '**' + SHOW_LAYER + '**' + HIDE_LAYER + '**' + SHOW_NAVIGATION_BAR + '**' + HIDE_NAVIGATION_BAR + '**' + SET_NAVIGATION_BAR_LAYOUT + '**' + SET_NAVIGATION_BAR_TITLE + '**' + SHOW_NAVIGATION_BAR_ALERT + '**' + SHOW_FULLSCREEN_VIDEO + '**' + SHOW_FULLSCREEN_IMAGE + '**' + SHOW_FULLSCREEN_TEXT + '**' + SHOW_FULLSCREEN_AUDIO + '**' + HIDE_FULLSCREEN + '**' + START_TIMEOUT + '**' + STOP_TIMEOUT + '**' + PLAY_AUDIO + '**' + STOP_AUDIO + '**' + SHOW_ACCESSIBILITY + '**' + GALLERY_NEXT + '**' + GALLERY_PREV;
		
		private var _type:String;
		private var _variablesDictionary:Dictionary = new Dictionary();
		
		public function ActionVO(type:String) 
		{
			if (_typesArray.match(type).length < 1) {
				Log.log('The action type \'' + type + '\' is not an accepted action type. Please ammend xml/structure.xml accordingly.', LogEvent.ERROR);
				return;
			}
			
			_type = type;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function addVariable(id:String, text:String):void
		{
			if (_variablesDictionary[id]) return;
			_variablesDictionary[id] = text;
		}
		
		public function getVariable(id:String):String
		{
			return ((_variablesDictionary[id]) ? _variablesDictionary[id] : '');
		}
		
	}

}