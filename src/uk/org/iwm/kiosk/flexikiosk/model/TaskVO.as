package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class TaskVO 
	{
		private var _actions:Vector.<ActionVO>;
		private var _overwrites:Dictionary;
		
		public function TaskVO() 
		{
			
		}
		
		public function processXML(taskXML:XML, supertaskXML:XML = null):void
		{
			_overwrites = new Dictionary(true)
			
			var xml:XML = taskXML;
			
			if (supertaskXML) {
				for (var i:int = 0; i < taskXML..overwrite.length(); i++) {
					for (var i2:int = 0; i2 < supertaskXML.descendants().length(); i2++) {
						if (supertaskXML.descendants()[i2] == taskXML..overwrite[i].@id) {
							supertaskXML.descendants()[i2].children()[0] = taskXML..overwrite[i].children()[0];
						}
					}
				}
				
				xml = supertaskXML;
			}
			
			_actions = new Vector.<ActionVO>()
			
			for (i = 0; i < xml..action.length(); i++) {
				
				var action_type:String = (xml..action[i].type.length() > 0) ? xml..action[i].type[0] : ActionVO.NONE;
				
				var avo:ActionVO = new ActionVO(action_type);
				
				for (i2 = 0; i2 < xml..action[i].children().length(); i2++) {
					avo.addVariable(xml..action[i].children()[i2].name().toString(), xml..action[i].children()[i2]);
				}
				
				_actions.push(avo);
			}
		}
		
		public function get actions():Vector.<ActionVO>
		{
			return _actions;
		}
		
	}

}