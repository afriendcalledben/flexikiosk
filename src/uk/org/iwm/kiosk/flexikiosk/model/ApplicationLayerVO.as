package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.geom.Rectangle;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import uk.org.iwm.kiosk.flexikiosk.model.BackgroundVO;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ApplicationLayerVO 
	{
		private var _id:String;
		private var _area:Rectangle;
		private var _background:BackgroundVO;
		private var _hidden:Boolean;
		
		public function ApplicationLayerVO() 
		{
			
		}
		
		public function processXML(xml:XML):void
		{
			_id = (xml.@id);
			
			var x:int = (xml.@x.length() > 0) ? xml.@x[0] : 0;
			var y:int = (xml.@y.length() > 0) ? xml.@y[0] : 0;
			var width:int = (xml.@width.length() > 0) ? xml.@width[0] : ApplicationVO.applicationWidth;
			var height:int = (xml.@height.length() > 0) ? xml.@height[0] : ApplicationVO.applicationHeight;
			
			_area = new Rectangle(x, y, width, height);
			
			_background = new BackgroundVO();
			if (xml..background.length() > 0) _background.processXML(xml..background[0]);
			
			_hidden = (xml..hidden.length() > 0) ? (xml..hidden[0] == 'true') : false;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get area():Rectangle
		{
			return _area;
		}
		
		public function get background():BackgroundVO
		{
			return _background;
		}
		
		public function get hidden():Boolean
		{
			return _hidden;
		}
		
	}

}