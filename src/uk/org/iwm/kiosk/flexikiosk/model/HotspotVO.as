package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import com.afriendcalledben.media.MediaManager;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class HotspotVO 
	{
		public static const IMAGE:String = 'HotspotVO.IMAGE';
		public static const SWF:String = 'HotspotVO.SWF';
		
		private var _type:String;
		private var _mediaId:String;
		private var _x:int;
		private var _y:int;
		private var _taskId:String;
		
		public function HotspotVO(type:String, mediaId:String, xPos:int, yPos:int, taskId:String = '') 
		{
			_type = type;
			_mediaId = mediaId;
			_x = xPos;
			_y = yPos;
			_taskId = taskId;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function get mediaId():String
		{
			return _mediaId;
		}
		
		public function get position():Point
		{
			return new Point(_x, _y);
		}
		
		public function get taskId():String
		{
			return _taskId;
		}
		
	}

}