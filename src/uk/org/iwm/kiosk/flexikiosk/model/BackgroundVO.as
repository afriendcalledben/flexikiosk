package uk.org.iwm.kiosk.flexikiosk.model 
{
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import com.afriendcalledben.media.MediaManager;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class BackgroundVO 
	{
		private var _color:Number;
		private var _alpha:Number;
		private var _image:BitmapData;
		private var _imageRepeat:Boolean;
		
		public function BackgroundVO(color:Number = 0, alpha:Number = 0, image:BitmapData = null, imageRepeat:Boolean = false) 
		{
			_color = color;
			_alpha = alpha;
			_image = image;
			_imageRepeat = imageRepeat;
		}
		
		public function processXML(xml:XML):void
		{
			_color = (xml..color.length() > 0) ? Number(xml..color[0]) : 0;
			_alpha = (xml..alpha.length() > 0) ? Number(xml..alpha[0]) : 0;
			var image:String = (xml..image.length() > 0) ? xml..image[0] : '';
			_imageRepeat = (xml..imageRepeat.length() > 0) ? Boolean(xml..imageRepeat) : false;
			if (image.length > 0) _image = MediaManager.getBitmapByID(image);
		}
		
		public function get color():Number
		{
			return _color;
		}
		
		public function get alpha():Number
		{
			return _alpha;
		}
		
		public function get image():BitmapData
		{
			return _image;
		}
		
		public function get imageRepeat():Boolean
		{
			return _imageRepeat;
		}
		
		public function setGraphics(gfx:Graphics):void
		{
			gfx.beginFill(_color, _alpha);
			if (_image) gfx.beginBitmapFill(_image, null, _imageRepeat, true);
		}
		
	}

}