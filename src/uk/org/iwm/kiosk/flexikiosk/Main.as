package uk.org.iwm.kiosk.flexikiosk
{
	import com.afriendcalledben.keyboard.KeyCode;
	import com.afriendcalledben.media.subtitles.SubtitlesManager;
	import com.lia.utils.SWFProfiler;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	import uk.org.iwm.kiosk.flexikiosk.log.LogSender;
	import uk.org.iwm.kiosk.iwmkioskoverlay.IWMKioskOverlay;
	
	import flash.events.Event;
	import com.greensock.events.LoaderEvent;
	import flash.events.InvokeEvent;
	
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.desktop.NativeApplication;
	import flash.net.SharedObject;
	import flash.filesystem.File;
	import flash.ui.Mouse;
	
	import uk.org.iwm.kiosk.flexikiosk.model.ApplicationLayerVO;
	import uk.org.iwm.kiosk.flexikiosk.view.ApplicationLayer;
	import uk.org.iwm.kiosk.flexikiosk.view.content.gallery.GallerySlide;
	import uk.org.iwm.kiosk.flexikiosk.view.content.gallery.Gallery;
	import uk.org.iwm.kiosk.flexikiosk.view.content.contentmap.ContentMap;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import uk.org.iwm.kiosk.flexikiosk.loader.ApplicationLoader;
	import com.afriendcalledben.media.MediaManager;
	import com.afriendcalledben.text.TextManager;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display.StageDisplayState;
	
	import flash.text.Font;
	import com.greensock.TweenMax;
	
	import flash.events.KeyboardEvent;
	
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Main extends Sprite 
	{
		private var _content:Sprite;
		private var _kioskScreen:IWMKioskOverlay;
		private var _mouseShown:Boolean = false;
		private var _profilerShown:Boolean = false;
		private var _logSender:LogSender;
		
		public function Main():void 
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvoke);
			
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			stage.align = StageAlign.TOP_LEFT
			
			//scrollRect = new Rectangle(0, 0, 1920, 1080);
			
			Log.addEventListener(LogEvent.LOG, onLog);
			
			//_logSender = new LogSender('iwm_logging');
		}
		
		private function onLog(e:LogEvent):void
		{
			trace('LOG');
			//_logSender.sendLogEvent(e);
		}
		
		private function onInvoke(e:InvokeEvent):void
		{
			_content = new Sprite();
			addChild(_content);
			
			_kioskScreen = new IWMKioskOverlay(stage.stageWidth, stage.stageHeight);
			addChild(_kioskScreen);
			
			var so:SharedObject = SharedObject.getLocal(NativeApplication.nativeApplication.applicationID);
			if (e.arguments.length > 0) {
				ApplicationVO.contentDirectory = e.arguments[0];
				so.data.contentDirectory = ApplicationVO.contentDirectory;
				so.flush();
				Log.log('Content directory was set to: ' + ApplicationVO.contentDirectory);
				TweenMax.delayedCall(1, checkContentDirectoryExists);
			} else if (so.data.contentDirectory) {
				ApplicationVO.contentDirectory = so.data.contentDirectory;
				Log.log('Content directory is: ' + ApplicationVO.contentDirectory);
				TweenMax.delayedCall(1, checkContentDirectoryExists);
			} else {
				Log.log('No content directory was set. Please select a content directory from your hard drive.');
				TweenMax.delayedCall(2, setContentDirectory);
            }
		}
		
		private function onSelectContentDirectory(e:Event):void
		{
			var file:File = File(e.currentTarget);
            file.removeEventListener(Event.SELECT, onSelectContentDirectory);
			var so:SharedObject = SharedObject.getLocal(NativeApplication.nativeApplication.applicationID);
			ApplicationVO.contentDirectory = file.nativePath;
			so.data.contentDirectory = ApplicationVO.contentDirectory;
			so.flush();
			Log.log('Content directory was set to: ' + ApplicationVO.contentDirectory);
			TweenMax.delayedCall(1, checkContentDirectoryExists);
		}
		
		private function checkContentDirectoryExists():void
		{
			ApplicationVO.contentDirectoryFile = File.userDirectory.resolvePath(ApplicationVO.contentDirectory);
			if (ApplicationVO.contentDirectoryFile.isDirectory) {
				if (!ApplicationManager.loaded) {
					Log.log('Content directory exists. Beginning application load.');
					loadContent();
				} else {
					Log.log('Content directory exists. Please restart the application. Press CTRL+Q to exit.');
				}
			} else {
				Log.log('CRITICAL ERROR: Content directory does not exist. Please select a content directory from your hard drive.', LogEvent.WARNING);
				TweenMax.delayedCall(2, setContentDirectory);
			}
		}
		
		private function loadContent():void
		{
			var applicationLoader:ApplicationLoader = new ApplicationLoader();
			applicationLoader.addEventListener(Event.COMPLETE, onApplicationContentLoaded);
			applicationLoader.load(ApplicationVO.contentDirectoryFile);
		}
		
		private function onApplicationContentLoaded(e:Event):void
		{
			TweenMax.delayedCall(10, buildApplication);
			
			NativeApplication.nativeApplication.activeWindow.x = ApplicationManager.windowRect.x;
			NativeApplication.nativeApplication.activeWindow.y = ApplicationManager.windowRect.y;
			NativeApplication.nativeApplication.activeWindow.width = ApplicationManager.windowRect.width;
			NativeApplication.nativeApplication.activeWindow.height = ApplicationManager.windowRect.height;
			
			scrollRect = new Rectangle(0, 0, ApplicationManager.windowRect.width, ApplicationManager.windowRect.height);
		}
		
		private function buildApplication():void
		{
			// SET UP THE APPLICATION
			
			if (ApplicationManager.fullScreen) stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			
			ApplicationManager.setupLayers(_content);
			
			_kioskScreen.hide();
			
			Mouse.hide();
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			//SWFProfiler.init(stage, this);
			//SWFProfiler.stop();
		}
		
		private function setContentDirectory():void
		{
			var file:File = new File();
            file.addEventListener(Event.SELECT, onSelectContentDirectory);
            file.browseForDirectory("Please select a content directory...");
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			// CTRL + C
			if (e.ctrlKey && e.keyCode == KeyCode.C) {
				if (_kioskScreen.isShowing) {
					_kioskScreen.hide();
				} else {
					_kioskScreen.show();
				}
			// CTRL + F
			} else if (e.ctrlKey && e.keyCode == KeyCode.F) {
				_kioskScreen.show();
				setContentDirectory();
			// CTRL + M
			} else if (e.ctrlKey && e.keyCode == KeyCode.M) {
				_mouseShown = !_mouseShown;
				if (_mouseShown) {
					Mouse.show();
				} else {
					Mouse.hide();
				}
			// CTRL + Q
			} else if (e.ctrlKey && e.keyCode == KeyCode.Q) {
				NativeApplication.nativeApplication.exit(0);
			} else if (e.ctrlKey && e.keyCode == KeyCode.S) {
				_profilerShown = !_profilerShown;
				if (_profilerShown) {
					SWFProfiler.show();
					SWFProfiler.start();
				} else {
					SWFProfiler.hide();
					SWFProfiler.stop();
				}
			}
		}
		
	}
	
}