package uk.org.iwm.kiosk.flexikiosk.manager 
{
	import air.update.ApplicationUpdater;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.system.Capabilities;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	import uk.org.iwm.kiosk.flexikiosk.model.ActionVO;
	import uk.org.iwm.kiosk.flexikiosk.model.ContentMapVO;
	import uk.org.iwm.kiosk.flexikiosk.model.ExternalSWFVO;
	import uk.org.iwm.kiosk.flexikiosk.model.GalleryVO;
	import uk.org.iwm.kiosk.flexikiosk.model.NavigationBarLayoutVO;
	import uk.org.iwm.kiosk.flexikiosk.model.TaskVO;
	import uk.org.iwm.kiosk.flexikiosk.view.content.ContentBase;
	import uk.org.iwm.kiosk.flexikiosk.view.content.fullscreen.*;
	import uk.org.iwm.kiosk.flexikiosk.view.navigation.NavigationBar;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import uk.org.iwm.kiosk.flexikiosk.view.ApplicationLayer;
	import uk.org.iwm.kiosk.flexikiosk.model.ApplicationLayerVO;
	import com.afriendcalledben.text.TextManager;
	import com.afriendcalledben.datetime.DateTimeFormatter;
	import com.afriendcalledben.timeout.TimeoutManager;
	import com.afriendcalledben.media.MediaManager;
	
	import flash.filesystem.File;
	import flash.net.FileReference;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ApplicationManager 
	{
		private static var _windowRect:Rectangle;
		private static var _fullScreen:Boolean;
		private static var _originalApplicationXML:XML;
		private static var _applicationXML:XML;
		private static var _applicationContent:Sprite;
		private static var _navigationBar:NavigationBar;
		private static var _fullscreen:FullScreenObject;
		private static var _loaded:Boolean = false;
		private static var _timeoutTask:String = '';
		
		public static function setXML(xml:XML):void
		{
			_originalApplicationXML = xml;
			_applicationXML = _originalApplicationXML.copy();
			
			var x:int = (xml..initialWindow.x.length() > 0) ? xml..initialWindow.x[0] : 0;
			var y:int = (xml..initialWindow.y.length() > 0) ? xml..initialWindow.y[0] : 0;
			var w:int = (xml..initialWindow.width.length() > 0) ? xml..initialWindow.width[0] : Capabilities.screenResolutionX;
			var h:int = (xml..initialWindow.height.length() > 0) ? xml..initialWindow.height[0] : Capabilities.screenResolutionY;
			_fullScreen = (xml..initialWindow.fullscreen.length() > 0) ? (xml..initialWindow.fullscreen == 'true') : false;
			
			_windowRect = new Rectangle(x, y, w, h);
		}
		
		public static function get windowRect():Rectangle
		{
			return _windowRect;
		}
		
		public static function get fullScreen():Boolean
		{
			return _fullScreen;
		}
		
		public static function setupLayers(content:Sprite):void
		{
			_applicationContent = content;
			
			TimeoutManager.setStage(content.stage);
			TimeoutManager.addEventListener(TimeoutManager.TIMEOUT, onTimeout);
			
			// Set up layers
			
			for (var i:int = 0; i < ApplicationVO.structureXML..layer.length(); i++)
			{
				var lvo:ApplicationLayerVO = new ApplicationLayerVO();
				lvo.processXML(_applicationXML..layer[i]);
				var layer:ApplicationLayer = new ApplicationLayer(lvo.area, lvo.background);
				layer.visible = !lvo.hidden;
				layer.hidden = lvo.hidden;
				layer.name = lvo.id;
				layer.x = lvo.area.x;
				layer.y = lvo.area.y;
				layer.setContent(ApplicationVO.structureXML..layer[i].content_type, ApplicationVO.structureXML..layer[i].content_id);
				_applicationContent.addChild(layer);
			}
			
			_navigationBar = new NavigationBar(_windowRect.width);
			_navigationBar.y = _windowRect.height - 80;
			setNavigationBarLayout('home_menu');
			_applicationContent.addChild(_navigationBar);
			
			_loaded = true;
		}
		
		public static function performTask(taskID:String, additionalVariables:Array = null):void
		{
			try {
				var xml:XML = _applicationXML..task.(attribute('id') == taskID)[0].copy();
			} catch (e:Error) {
				Log.log('The task \'' + taskID + '\' could not be found in xml/structure.xml. Please make sure the task is defined.', LogEvent.ERROR);
				return;
			}
			var sxml:XML;
			
			if (xml..super_task.length() > 0) {
				sxml = _applicationXML..task.(attribute('id') == xml..super_task[0])[0].copy();
			}
			
			var tvo:TaskVO = new TaskVO();
			tvo.processXML(xml, sxml);
			
			for (var i:int = 0; i < tvo.actions.length; i++) {
				var avo:ActionVO = tvo.actions[i];
				if (additionalVariables) {
					for (var i2:int = 0; i2 < additionalVariables.length; i2++) {
						avo.addVariable(additionalVariables[i2][0], additionalVariables[i2][1]);
					}
				}
				performAction(avo);
			}
		}
		
		public static function performAction(avo:ActionVO):void
		{
			var layer:ApplicationLayer;
				switch (avo.type) 
				{
					case ActionVO.SET_CONTENT :
						layer = ApplicationLayer(_applicationContent.getChildByName(avo.getVariable('layer_id')));
						layer.setContent(avo.getVariable('content_type'), avo.getVariable('content_id'));
						break;
					case ActionVO.SHOW_LAYER :
						layer = ApplicationLayer(_applicationContent.getChildByName(avo.getVariable('layer_id')));
						layer.show();
						break;
					case ActionVO.HIDE_LAYER :
						layer = ApplicationLayer(_applicationContent.getChildByName(avo.getVariable('layer_id')));
						layer.hide();
						break;
					case ActionVO.SHOW_NAVIGATION_BAR :
						showNavigationBar();
						break;
					case ActionVO.HIDE_NAVIGATION_BAR :
						hideNavigationBar();
						break;
					case ActionVO.SET_NAVIGATION_BAR_LAYOUT :
						setNavigationBarLayout(avo.getVariable('navigation_bar_layout'));
						break;
					case ActionVO.SET_NAVIGATION_BAR_TITLE :
						setNavigationBarTitle(avo.getVariable('text'));
						break;
					case ActionVO.SHOW_NAVIGATION_BAR_ALERT :
						showNavigationBarAlert(avo.getVariable('message'));
						break;
					case ActionVO.SHOW_FULLSCREEN_IMAGE :
						showFullscreenImage(avo.getVariable('image'), avo.getVariable('close_task'));
						break;
					case ActionVO.SHOW_FULLSCREEN_VIDEO :
						showFullscreenVideo(avo.getVariable('video'), avo.getVariable('accessibility'), null, avo.getVariable('close_task'));
						break;
					case ActionVO.SHOW_FULLSCREEN_AUDIO :
						showFullscreenAudio(avo.getVariable('audio'), avo.getVariable('accessibility'), avo.getVariable('image'), avo.getVariable('close_task'));
						break;
					case ActionVO.PLAY_AUDIO :
						playAudio(avo.getVariable('audio'));
						break;
					case ActionVO.HIDE_FULLSCREEN :
						hideFullscreen();
						break;
					case ActionVO.START_TIMEOUT :
						startTimeout(Number(avo.getVariable('time')), avo.getVariable('task_id'));
						break;
					case ActionVO.STOP_TIMEOUT :
						stopTimeout();
						break;
					case ActionVO.SHOW_ACCESSIBILITY :
						showAccessibility(avo.getVariable('layer_id'));
						break;
					case ActionVO.GALLERY_PREV :
						previousGallery(avo.getVariable('layer_id'));
						break;
					case ActionVO.GALLERY_NEXT :
						nextGallery(avo.getVariable('layer_id'));
						break;
				}
		}
		
		public static function previousGallery(layerId:String):void
		{
			if (layerId == null) return;
			var layer:ApplicationLayer = ApplicationLayer(_applicationContent.getChildByName(layerId));
			var content:ContentBase = layer.getContent();
			content.previousGallery();
		}
		
		public static function nextGallery(layerId:String):void
		{
			if (layerId == null) return;
			var layer:ApplicationLayer = ApplicationLayer(_applicationContent.getChildByName(layerId));
			var content:ContentBase = layer.getContent();
			content.nextGallery();
		}
		
		public static function showAccessibility(layerId:String):void
		{
			if (layerId == null) return;
			var layer:ApplicationLayer = ApplicationLayer(_applicationContent.getChildByName(layerId));
			var content:ContentBase = layer.getContent();
			content.showAccessibility();
		}
		
		public static function showNavigationBar():void
		{
			if (_navigationBar) _navigationBar.show();
		}
		
		public static function hideNavigationBar():void
		{
			if (_navigationBar) _navigationBar.hide();
		}
		
		public static function setNavigationBarLayout(id:String):void
		{
			var navbarLayoutXML:XML = _applicationXML..navigation_bar_layout.(attribute('id') == id)[0];
			
			if (!navbarLayoutXML) throw new Error('Navigation Bar Layout \'' + id + '\' not found!');
			
			var vo:NavigationBarLayoutVO = new NavigationBarLayoutVO();
			vo.processXML(navbarLayoutXML);
			
			if (_navigationBar) _navigationBar.setLayout(vo);
		}
		
		public static function setNavigationBarTitle(id:String):void
		{
			if (_navigationBar) _navigationBar.addTitle(id);
		}
		
		public static function showNavigationBarAlert(message:String):void
		{
			if (_navigationBar) _navigationBar.showAlert(TextManager.getText(message));
		}
		
		public static function showFullscreenImage(image_id:String, close_task:String = ''):void
		{
			_fullscreen = new FullScreenImage(_windowRect.width, _windowRect.height, image_id);
			_fullscreen.closeTask = close_task;
			_applicationContent.addChild(_fullscreen);
			_fullscreen.show();
		}
		
		public static function showFullscreenVideo(video_id:String, subtitles_id:String = '', video_rect:Rectangle = null, close_task:String = ''):void
		{
			_fullscreen = new FullScreenVideo(_windowRect.width, _windowRect.height, video_id, subtitles_id, video_rect);
			if (close_task) _fullscreen.closeTask = close_task;
			_applicationContent.addChild(_fullscreen);
			_fullscreen.show();
		}
		
		public static function showFullscreenAudio(audio_id:String, text_id:String = '', image_id:String = '', close_task:String = ''):void
		{
			_fullscreen = new FullScreenAudio(_windowRect.width, _windowRect.height, audio_id, text_id, image_id);
			_fullscreen.closeTask = close_task;
			_applicationContent.addChild(_fullscreen);
			_fullscreen.show();
		}
		
		public static function playAudio(audio_id:String):void
		{
			var snd:Sound = MediaManager.getAudioByID(audio_id);
			snd.play();
		}
		
		public static function hideFullscreen():void
		{
			if (_fullscreen) {
				if (_applicationContent.contains(_fullscreen)) {
					_fullscreen.hide();
					_fullscreen = null;
				}
			}
		}
		
		public static function startTimeout(time:Number, taskId:String):void
		{
			_timeoutTask = taskId;
			TimeoutManager.setTimeout(time);
			TimeoutManager.activate();
		}
		
		public static function stopTimeout():void
		{
			_timeoutTask = '';
			TimeoutManager.deactivate();
		}
		
		private static function onTimeout(e:Event):void
		{
			if (_timeoutTask.length > 0) performTask(_timeoutTask);
		}
		
		public static function getGalleryByID(id:String):GalleryVO
		{
			var galleryXML:XML = _applicationXML..gallery.(attribute('id') == id)[0];
			
			if (!galleryXML) throw new Error('Gallery \'' + id + '\' not found!');
			
			var galleryVO:GalleryVO = new GalleryVO();
			galleryVO.processXML(galleryXML);
			return galleryVO;
		}
		
		public static function getContentMapByID(id:String):ContentMapVO
		{
			var contentMapXML:XML = _applicationXML..content_map.(attribute('id') == id)[0];
			
			if (!contentMapXML) throw new Error('ContentMap \'' + id + '\' not found!');
			
			var contentMapVO:ContentMapVO = new ContentMapVO();
			contentMapVO.processXML(contentMapXML);
			return contentMapVO;
		}
		
		public static function getExternalSWFByID(id:String):ExternalSWFVO
		{
			var externalSWFXML:XML = _applicationXML..external_swf.(attribute('id') == id)[0];
			
			if (!externalSWFXML) throw new Error('ExternalSWF \'' + id + '\' not found!');
			
			var externalSWFVO:ExternalSWFVO = new ExternalSWFVO();
			externalSWFVO.processXML(externalSWFXML);
			return externalSWFVO;
		}
		
		public static function updateHotspot(contentmapId:String, hotspotId:int, x:int, y:int):void
		{
			var contentMapXML:XML = _applicationXML..content_map.(attribute('id') == contentmapId)[0];
			
			if (!contentMapXML) throw new Error('ContentMap \'' + contentmapId + '\' not found!');
			
			contentMapXML..hotspot[hotspotId].x = x;
			contentMapXML..hotspot[hotspotId].y = y;
		}
		
		public static function saveXML():void
		{
				var backupPath:String = ApplicationVO.contentDirectory + '\\xml\\structure_OLD_' + DateTimeFormatter.getDateString(new Date(), '') + '_' + DateTimeFormatter.getTimeString(new Date(), '') + '.xml';
				var newPath:String = ApplicationVO.contentDirectory + '\\xml\\structure.xml';
				
				var f:File = new File(backupPath); 
				var stream:FileStream = new FileStream();
				stream.open(f, FileMode.WRITE);      
				stream.writeUTFBytes(_originalApplicationXML);
				stream.close();
				
				f = new File(newPath); 
				stream = new FileStream();
				stream.open(f, FileMode.WRITE);      
				stream.writeUTFBytes(_applicationXML);
				stream.close();
		}
		
		public static function get loaded():Boolean
		{
			return _loaded;
		}
		
		private static function getRectangleFromXML(xml:XML):Rectangle
		{
			var x:Number = (xml..x.length() > 0) ? Number(xml..x[0]) : 0;
			var y:Number = (xml..y.length() > 0) ? Number(xml..y[0]) : 0;
			var width:Number = (xml..width.length() > 0) ? Number(xml..width[0]) : 0;
			var height:Number = (xml..height.length() > 0) ? Number(xml..height[0]) : 0;
			return new Rectangle(x, y, width, height);
		}
		
		private static function getRectangleFromString(value:String, delimiter:String = ','):Rectangle
		{
			value = value.split(' ').join('');
			var arr:Array = value.split(delimiter);
			return new Rectangle(arr[0], arr[1], arr[2], arr[3]);
		}
		
	}

}