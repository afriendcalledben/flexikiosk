package uk.org.iwm.kiosk.flexikiosk.view.ui 
{
	import com.afriendcalledben.display.ShapeRect;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class IndicatorTime extends Sprite
	{
		private var _bar:ShapeRect;
		
		public function IndicatorTime(w:Number, col:Number = 0xFFFFFF) 
		{
			var bg:ShapeRect = new ShapeRect(w, 10, 0);
			addChild(bg);
			
			_bar = new ShapeRect(w, 10, col);
			_bar.scaleX = 0;
			addChild(_bar);
		}
		
		public function update(value:Number):void
		{
			_bar.scaleX = value;
		}
		
	}

}