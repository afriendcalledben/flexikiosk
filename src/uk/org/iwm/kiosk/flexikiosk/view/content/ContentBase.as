package uk.org.iwm.kiosk.flexikiosk.view.content 
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ContentBase extends Sprite
	{
		
		public function ContentBase() 
		{
			
		}
		
		public function show():void
		{
			
		}
		
		public function hide():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
		
		public function previousGallery():void
		{
			
		}
		
		public function nextGallery():void
		{
			
		}
		
		public function showAccessibility():void
		{
			
		}
	}

}