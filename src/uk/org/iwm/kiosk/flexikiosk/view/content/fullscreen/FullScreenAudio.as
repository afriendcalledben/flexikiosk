package uk.org.iwm.kiosk.flexikiosk.view.content.fullscreen 
{
	import com.afriendcalledben.display.ShapeRect;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.afriendcalledben.services.BitmapResizer;
	import com.afriendcalledben.media.MediaManager;
	import com.afriendcalledben.text.TextManager;
	import com.afriendcalledben.text.TextPanel;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import uk.org.iwm.kiosk.flexikiosk.view.ui.IndicatorTime;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class FullScreenAudio extends FullScreenObject
	{
		private var _indicator:IndicatorTime;
		private var _audio:Sound;
		private var _missingAudio:Boolean = false;
		private var _soundChannel:SoundChannel;
		private var _textOverlay:Sprite;
		
		public function FullScreenAudio(w:int, h:int, audio_id:String, text_id:String = '', image_id:String = '') 
		{
			super(w, h);
			
			if (image_id.length > 0) {
				var bmp:Bitmap = new Bitmap(BitmapResizer.getImageBitmapDataWithinRect(MediaManager.getBitmapByID(image_id), new Rectangle(0, 0, w, h)));
				_content.addChild(bmp);
			}
			
			if (text_id.length > 0) {
				_textOverlay = new Sprite();
				var txt:String = TextManager.getText(text_id);
				var bg:ShapeRect = new ShapeRect(w, h, 0, 0.85);
				_textOverlay.addChild(bg);
				var tp:TextPanel = new TextPanel(txt, w - 400);
				tp.x = 200;
				tp.y = (h * .5) - (tp.textHeight * .5);
				_textOverlay.addChild(tp);
				_content.addChild(_textOverlay);
			}
			
			_indicator = new IndicatorTime(w);
			_indicator.y = _height - 10;
			_content.addChild(_indicator);
			
			_audio = MediaManager.getAudioByID(audio_id);
			if (_audio == null) {
				_missingAudio = true;
			}
			
			addCloseButton();
		}
		
		override public function show():void
		{
			TweenMax.to(this, 0.3, { alpha:1, onComplete:showComplete } );
		}
		
		private function showComplete():void
		{
			if (!_missingAudio) {
			_soundChannel = _audio.play();
			_soundChannel.addEventListener(Event.SOUND_COMPLETE, onClose);
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			}
		}
		
		override protected function onClose(e:Event = null):void
		{
			removeEventListener(Event.ENTER_FRAME, update);
			if (_soundChannel) _soundChannel.stop();
			super.onClose(e);
		}
		
		override protected function onAccessibility(e:Event = null):void
		{
			if (_textOverlay) {
				TweenMax.to(_textOverlay, 0.5, { alpha: ((_textOverlay.alpha < 0.5) ? 1 : 0) } );
			}
		}
		
		public function update(e:Event):void
		{
			var position:Number = 1 / _audio.length * _soundChannel.position;
			_indicator.update(position);
		}
		
		override public function destroy():void
		{
			if (_soundChannel) _soundChannel.stop();
			if (parent) parent.removeChild(this);
		}
		
	}

}