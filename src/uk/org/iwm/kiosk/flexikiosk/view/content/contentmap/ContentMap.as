package uk.org.iwm.kiosk.flexikiosk.view.content.contentmap 
{
	import com.afriendcalledben.display.Background;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import com.greensock.BlitMask;
	import flash.geom.Rectangle;
	import com.greensock.TweenMax;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.ThrowPropsPlugin;
	import flash.text.TextFormat;
	import flash.utils.getTimer;
	import com.afriendcalledben.media.MediaManager;
	import uk.org.iwm.kiosk.flexikiosk.model.ContentMapVO;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	import uk.org.iwm.kiosk.flexikiosk.model.HotspotVO;
	import uk.org.iwm.kiosk.flexikiosk.view.content.ContentBase;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ContentMap extends ContentBase
	{
		private var _width:int;
		private var _height:int;
		private var _bounds:Rectangle;
		private var _contentMap:Sprite;
		private var _contentMapId:String;
		private var _hotspots:Sprite;
		private var _blitMask:BlitMask;
		private var _xAxisEnabled:Boolean = false;
		private var _yAxisEnabled:Boolean = false;
		private var _hotspotTasks:Array;
		private var _currentHotspot:Hotspot;
		private var _editMode:Boolean = false;
		private var _editStatusLabel:TextLabel;
		private var t1:uint, t2:uint, t3:uint, x1:Number, x2:Number, xOverlap:Number, xOffset:Number, xOrig:Number, y1:Number, y2:Number, yOverlap:Number, yOffset:Number, yOrig:Number;
		
		public function ContentMap(w:int, h:int) 
		{
			_width = w;
			_height = h;
			_bounds = new Rectangle(0, 0, _width, _height);
			
			TweenPlugin.activate([ThrowPropsPlugin]);
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			if (_xAxisEnabled || _yAxisEnabled)
			{
				if (stage) stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			}
			if (stage) stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		public function setContent(cmvo:ContentMapVO):void
		{
			_contentMap = new Sprite();
			_contentMapId = cmvo.id;
			cmvo.mapBackground.setGraphics(_contentMap.graphics);
			_contentMap.graphics.drawRect(0, 0, cmvo.mapArea.width, cmvo.mapArea.height);
			addChild(_contentMap);
			
			_xAxisEnabled = (_contentMap.width > _bounds.width);
			_yAxisEnabled = (_contentMap.height > _bounds.height);
			
			if (_xAxisEnabled || _yAxisEnabled)
			{
				_blitMask = new BlitMask(_contentMap, 0, 0, _width, _height, false);
				_blitMask.bitmapMode = false;
				TweenMax.delayedCall(1, _blitMask.update, [null, true]);
				if (stage) stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			}
			
			_hotspots = new Sprite();
			_contentMap.addChild(_hotspots);
			_hotspotTasks = new Array();
			
			for (var i:int = 0; i < cmvo.getHotspots().length; i++) {
				var hsvo:HotspotVO = cmvo.getHotspots()[i];
				var hotspot:Hotspot = new Hotspot(hsvo.taskId);
				hotspot.name = 'hotspot' + i;
				hotspot.x = hsvo.position.x;
				hotspot.y = hsvo.position.y;
				if (hsvo.type == HotspotVO.IMAGE) {
					var bd:BitmapData = MediaManager.getBitmapByID(hsvo.mediaId);
					hotspot.addImageIcon(bd);
				} else if (hsvo.type == HotspotVO.SWF) {
					var mc:MovieClip = MediaManager.getSwfByID(hsvo.mediaId);
					hotspot.addSwfIcon(mc);
				}
				hotspot.addEventListener(MouseEvent.MOUSE_DOWN, onHotspotClick);
				_hotspots.addChild(hotspot);
				_hotspotTasks.push( cmvo.getHotspots()[i].taskId );
			}
			
			var tf:TextFormat = new TextFormat('Arial', 30, 0xFFFFFF, true);
			
			_editStatusLabel = new TextLabel('EDIT MODE ON: Drag and drop the hotspots to there correct positions', tf);
			_editStatusLabel.x = _editStatusLabel.y = 20;
			_editStatusLabel.visible = false;
			addChild(_editStatusLabel);
		}
		
		private function onHotspotClick(e:MouseEvent):void
		{
			var id:int = int(e.currentTarget.name.slice(7));
			_currentHotspot = Hotspot(e.currentTarget);
			if (!_editMode) {
				if (_hotspotTasks[id] != '') {
					ApplicationManager.performTask(_hotspotTasks[id]);
				}
			} else {
				removeEventListener(Event.ENTER_FRAME, moveHotspot);
				addEventListener(Event.ENTER_FRAME, moveHotspot);
				stage.removeEventListener(MouseEvent.MOUSE_UP, endMoveHotspot);
				stage.addEventListener(MouseEvent.MOUSE_UP, endMoveHotspot);
				selectHotspot(id);
			}
		}
		
		private function moveHotspot(e:Event):void
		{
			_currentHotspot.x = mouseX;
			_currentHotspot.y = mouseY;
		}
		
		private function endMoveHotspot(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, moveHotspot);
			stage.removeEventListener(MouseEvent.MOUSE_UP, endMoveHotspot);
		}
		
		private function mouseDownHandler(event:MouseEvent):void
		{
			if (!_editMode) {
				//if (_isDown || !_allowDragging) return;
				//_isDown = true;
				//MonsterDebugger.trace(this, 'TOUCH DOWN');
				TweenMax.killTweensOf(_contentMap);
				if (_xAxisEnabled) {
					x1 = x2 = xOrig = _contentMap.x;
					xOffset = this.mouseX - _contentMap.x;
					xOverlap = Math.max(0, _contentMap.width - _bounds.width);
				}
				if (_yAxisEnabled) {
					y1 = y2 = yOrig = _contentMap.y;
					yOffset = this.mouseY - _contentMap.y;
					yOverlap = Math.max(0, _contentMap.height - _bounds.height);
				}
				t1 = t2 = t3 = getTimer();
				
				removeEventListener(Event.ENTER_FRAME, mouseMoveHandler);
				addEventListener(Event.ENTER_FRAME, mouseMoveHandler);
				stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				
				mouseMoveHandler();
			}
		}

		private function mouseMoveHandler(event:Event = null):void
		{
			if (!_editMode) {
				//if (!_isDown || !_allowDragging) return;
				if (_xAxisEnabled) {
					var x:Number = this.mouseX - xOffset;
					var nx:Number;
					if (x > _bounds.left) {
						nx = (x + _bounds.left) * 0.2;
					} else if (x < _bounds.left - xOverlap) {
						nx = - xOverlap - (((_bounds.left - xOverlap) - x) * 0.2);
						//_timeline.x = x + ((_bounds.left - xOverlap) - x)
					} else {
						nx = x;
					}
					_contentMap.x += (nx - _contentMap.x) / 3;
				}
				if (_yAxisEnabled) {
					var y:Number = this.mouseY - yOffset;
					var ny:Number;
					if (y > _bounds.top) {
						ny = (y + _bounds.top) * 0.2;
					} else if (y < _bounds.top - yOverlap) {
						ny = - yOverlap - (((_bounds.top - yOverlap) - y) * 0.2);
						//_timeline.x = x + ((_bounds.left - xOverlap) - x)
					} else {
						ny = y;
					}
					if (int(_contentMap.x) != int(nx) && int(_contentMap.y) != int(ny)) {
						_blitMask.bitmapMode = true;
					}
					_contentMap.y += (ny - _contentMap.y) / 3;
				}
				update();
				var t:uint = getTimer();
				//if the frame rate is too high, we won't be able to track the velocity as well, so only update the values 20 times per second
				if (t - t2 > 50) {
					x2 = x1;
					y2 = y1;
					x1 = _contentMap.x;
					y1 = _contentMap.y;
					t2 = t1;
					t1 = t;
				}
			}
		}

		private function mouseUpHandler(event:MouseEvent):void
		{
			//MonsterDebugger.trace(this, 'TOUCH UP');
			stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			removeEventListener(Event.ENTER_FRAME, mouseMoveHandler);
			//if (!_isDown || !_allowDragging) return;
			//_isDown = false;
			var time:Number = (getTimer() - t2) / 1000;
			var xVelocity:Number = (_contentMap.x - x2) / time;
			var yVelocity:Number = (_contentMap.y - y2) / time;
			if (isNaN(xVelocity) && isNaN(yVelocity)) { 
				complete();
			} else {
				ThrowPropsPlugin.to(_contentMap, {throwProps:{ x: { velocity:xVelocity, max:_bounds.left, min:_bounds.left - xOverlap, resistance:300 }, y: { velocity:yVelocity, max:_bounds.top, min:_bounds.top - yOverlap, resistance:300 } }, onUpdate:update, onComplete:complete }, 2, 0.2, 0.2);
			}
		}
		
		private function update():void
		{
			var diffX:Number = _contentMap.x - xOrig;
			xOrig = _contentMap.x;
			yOrig = _contentMap.y;
			_blitMask.update();
		}
		
		private function complete():void
		{
			//MonsterDebugger.trace(this, 'DRAG COMPLETE');
			_blitMask.update();
			_blitMask.bitmapMode = false;
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			var moveDist:int = (e.shiftKey) ? 10 : 1;
			if (e.ctrlKey && e.keyCode == 69) {
				_editMode = !_editMode;
				if (_editMode) startEditMode();
				if (!_editMode) endEditMode();
			} else if (e.keyCode == 37) { // LEFT
				if (_currentHotspot && _editMode) _currentHotspot.x-=moveDist;
			} else if (e.keyCode == 38) { // UP
				if (_currentHotspot && _editMode) _currentHotspot.y-=moveDist;
			} else if (e.keyCode == 39) { // RIGHT
				if (_currentHotspot && _editMode) _currentHotspot.x+=moveDist;
			} else if (e.keyCode == 40) { // DOWN
				if (_currentHotspot && _editMode) _currentHotspot.y+=moveDist;
			} else if (e.ctrlKey && e.keyCode == 83) {
				for (var i:int = 0; i < _hotspots.numChildren; i++) {
					var hp:Hotspot = Hotspot(_hotspots.getChildAt(i));
					ApplicationManager.updateHotspot(_contentMapId, i, hp.x, hp.y); 
				}
				ApplicationManager.saveXML();
			}
		}
		
		private function startEditMode():void
		{
			_editStatusLabel.visible = true;
			for (var i:int = 0; i < _hotspots.numChildren; i++) {
				var hp:Hotspot = Hotspot(_hotspots.getChildAt(i));
				hp.editable = true;
			}
		}
		
		private function endEditMode():void
		{
			_editStatusLabel.visible = false;
			for (var i:int = 0; i < _hotspots.numChildren; i++) {
				var hp:Hotspot = Hotspot(_hotspots.getChildAt(i));
				hp.editable = false;
			}
		}
		
		private function selectHotspot(id:Number = NaN):void
		{
			for (var i:int = 0; i < _hotspots.numChildren; i++) {
				var hp:Hotspot = Hotspot(_hotspots.getChildAt(i));
				if (i == id) {
					//hp.select();
				} else {
					//hp.deselect();
				}
			}
		}
		
	}

}