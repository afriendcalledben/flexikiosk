package uk.org.iwm.kiosk.flexikiosk.view.content.fullscreen 
{
	import com.afriendcalledben.display.ShapeRect;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class FullScreenCloseButton extends Sprite
	{
		[Embed(source="../../../../../../../../../assets/img/close_icon.png")]
		private static var Gfx_close:Class;
		
		public function FullScreenCloseButton(w:int, h:int) 
		{
			var closeBtn:Bitmap = new Gfx_close();
			closeBtn.x = (w * .5) - (closeBtn.width * .5);
			closeBtn.y = h - 70;
			addChild(closeBtn);
			
			var btn:ShapeRect = new ShapeRect(w, h, 0, 0);
			addChild(btn);
		}
		
	}

}