package uk.org.iwm.kiosk.flexikiosk.view.content.contentmap 
{
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Hotspot extends Sprite
	{
		public static const LABEL_TOP:String = 'Hotspot.LABEL_TOP';
		public static const LABEL_BOTTOM:String = 'Hotspot.LABEL_TOP';
		public static const LABEL_LEFT:String = 'Hotspot.LABEL_TOP';
		public static const LABEL_RIGHT:String = 'Hotspot.LABEL_TOP';
		
		private var _editLabel:TextLabel;
		
		public function Hotspot(editid:String = '') 
		{
			if (editid == null) editid = '';
			_editLabel = new TextLabel(editid, new TextFormat('Arial', 12, 0, true), 0xFFFFFF);
			_editLabel.visible = false;
			addChild(_editLabel);
		}
		
		public function addImageIcon(bd:BitmapData = null, width:Number = NaN, height:Number = NaN, offsetx:int = 0, offsety:int = 0):void
		{
			if (bd) {
				var bmp:Bitmap = new Bitmap(bd.clone());
				bmp.x = offsetx;
				bmp.y = offsety;
				addChildAt(bmp, 0);
			}
			
			var w:Number = (!isNaN(width)) ? width : ((bmp) ? bmp.width : 100);
			var h:Number = (!isNaN(height)) ? height : ((bmp) ? bmp.width : 100);
			
			var btn:ShapeRect = new ShapeRect(w, h, 0, 0);
			addChild(btn);
		}
		
		public function addSwfIcon(mc:MovieClip, width:Number = NaN, height:Number = NaN, offsetx:int = 0, offsety:int = 0):void
		{
			if (mc) {
				mc.x = offsetx;
				mc.y = offsety;
				addChildAt(mc, 0);
				mc.play();
			}
			
			var w:Number = (!isNaN(width)) ? width : ((mc) ? mc.width : 100);
			var h:Number = (!isNaN(height)) ? height : ((mc) ? mc.width : 100);
			
			var btn:ShapeRect = new ShapeRect(w, h, 0, 0);
			addChild(btn);
		}
		
		public function get editable():Boolean
		{
			return _editLabel.visible;
		}
		
		public function set editable(value:Boolean):void
		{
			_editLabel.visible = value;
			if (!value) deselect();
		}
		
		public function select():void
		{
			this.filters = [new GlowFilter(0xFF0000, 1, 5, 5, 100)];
		}
		
		public function deselect():void
		{
			this.filters = [];
		}
	}

}