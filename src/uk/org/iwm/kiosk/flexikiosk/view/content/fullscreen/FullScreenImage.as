package uk.org.iwm.kiosk.flexikiosk.view.content.fullscreen 
{
	import flash.display.Bitmap;
	import com.afriendcalledben.services.BitmapResizer;
	import com.afriendcalledben.media.MediaManager;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class FullScreenImage extends FullScreenObject
	{
		
		public function FullScreenImage(w:int, h:int, image_id:String) 
		{
			super(w, h);
			
			var bmp:Bitmap = new Bitmap(BitmapResizer.getImageBitmapDataWithinRect(MediaManager.getBitmapByID(image_id), new Rectangle(0, 0, w, h)));
			_content.addChild(bmp);
			
			addCloseButton();
		}
		
	}

}