package uk.org.iwm.kiosk.flexikiosk.view.content.fullscreen 
{
	import com.afriendcalledben.media.subtitles.SubtitlesManager;
	import com.afriendcalledben.media.subtitles.SubtitlesView;
	import com.afriendcalledben.media.VideoView;
	import com.afriendcalledben.media.MediaManager;
	import com.afriendcalledben.style.Padding;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.greensock.TweenMax;
	import com.afriendcalledben.events.VideoEvent;
	import flash.geom.Rectangle;
	import flash.system.System;
	import uk.org.iwm.kiosk.flexikiosk.view.ui.IndicatorTime;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class FullScreenVideo extends FullScreenObject
	{
		private var _indicator:IndicatorTime;
		private var _videoPath:String;
		private var _videoView:VideoView;
		private var _shown:Boolean = false;
		private var _subtitlesManager:SubtitlesManager;
		private var _subtitlesView:SubtitlesView;
		
		public function FullScreenVideo(w:int, h:int, video_id:String, subtitles_id:String = '', rect:Rectangle = null, bsl:Boolean = false) 
		{
			super(w, h);
			
			var vidRect:Rectangle = (rect) ? rect : new Rectangle(0, 0, w, h);
			
			_videoView = new VideoView(MediaManager.getVideoPathByID(video_id), vidRect.width, vidRect.height);
			_videoView.addEventListener(VideoEvent.VIDEO_CUED, onCued);
			_videoView.addEventListener(VideoEvent.VIDEO_STOPPED, onClose);
			_videoView.x = vidRect.x;
			_videoView.y = vidRect.y;
			addChild(_videoView);
			
			if (subtitles_id) {
				_subtitlesManager = new SubtitlesManager();
				_subtitlesManager.addEventListener(Event.COMPLETE, onSubtitlesLoaded);
				_subtitlesManager.load(MediaManager.getSubtitlesPathByID(subtitles_id));
				
				_subtitlesView = new SubtitlesView(w, h, new Padding(0, 0, 100, 0));
				addChild(_subtitlesView);
			}
			
			_indicator = new IndicatorTime(w);
			_indicator.y = _height - 10;
			addChild(_indicator);
			
			addCloseButton();
		}
		
		private function onCued(e:Event):void
		{
			if (_shown) _videoView.start();
		}
		
		private function onSubtitlesLoaded(e:Event):void
		{
			
		}
		
		override public function show():void
		{
			TweenMax.to(this, 0.3, { alpha:1, onComplete:showComplete } );
		}
		
		private function showComplete():void
		{
			_shown = true;
			_videoView.cueVideo();
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		override protected function onClose(e:Event = null):void
		{
			removeEventListener(Event.ENTER_FRAME, update);
			super.onClose(e);
		}
		
		override protected function onAccessibility(e:Event = null):void
		{
			_subtitlesView.visible = !_subtitlesView.visible;
		}
		
		public function update(e:Event):void
		{
			var position:Number = 1 / _videoView.duration * _videoView.position;
			_indicator.update(position);
			if (_subtitlesManager) {
			var txt:String = _subtitlesManager.getTitles(_videoView.position);
			_subtitlesView.setSubtitles(txt);
			}
		}
		
		override public function destroy():void
		{
			if (_videoView) _videoView.dispose();
			if (parent) parent.removeChild(this);
			System.gc();
		}
		
	}

}