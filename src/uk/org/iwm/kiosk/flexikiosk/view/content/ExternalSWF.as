package uk.org.iwm.kiosk.flexikiosk.view.content 
{
	import com.afriendcalledben.display.Background;
	import com.afriendcalledben.text.TextLabel;
	import com.afriendcalledben.text.TextManager;
	import com.greensock.loading.SWFLoader;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import com.greensock.BlitMask;
	import flash.geom.Rectangle;
	import com.greensock.TweenMax;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.ThrowPropsPlugin;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	import com.afriendcalledben.media.MediaManager;
	import uk.org.iwm.kiosk.flexikiosk.model.ContentMapVO;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	import uk.org.iwm.kiosk.flexikiosk.model.ExternalSWFVO;
	import uk.org.iwm.kiosk.flexikiosk.view.content.ContentBase;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ExternalSWF extends ContentBase
	{
		private var _mc:MovieClip;
		private var _xml:XML;
		private var _width:int;
		private var _height:int;
		private var _bounds:Rectangle;
		private var t1:uint, t2:uint, t3:uint, x1:Number, x2:Number, xOverlap:Number, xOffset:Number, xOrig:Number, y1:Number, y2:Number, yOverlap:Number, yOffset:Number, yOrig:Number;
		private var _tasks:Array;
		private var _taskDictionary:Dictionary = new Dictionary();
		
		public function ExternalSWF(w:int, h:int) 
		{
			_width = w;
			_height = h;
			_bounds = new Rectangle(0, 0, _width, _height);
			
			//addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public function setContent(esvo:ExternalSWFVO):void
		{
			_mc = MediaManager.getSwfByID(esvo.swf);
			addChild(_mc);
			var obj:Object;
			// Text 
			for (var i:int = 0; i < esvo.getText().length; i++) {
				obj = esvo.getText()[i];
				try {
					_mc['setText'](obj.id, TextManager.getText(obj.text));
				} catch(e:Error) { }
			}
			// Image 
			for (i = 0; i < esvo.getImages().length; i++) {
				obj = esvo.getImages()[i];
				trace(obj.image);
				try {
					_mc['setImage'](obj.id, MediaManager.getBitmapByID(obj.image));
				} catch(e:Error) { }
			}
			_tasks = esvo.getTasks();
			for (i = 0; i < _tasks.length; i++)
			{
				_mc.removeEventListener(_tasks[i].event, onFunction);
				_mc.addEventListener(_tasks[i].event, onFunction);
			}
		}
		
		private function onFunction(e:Event):void
		{
			for (var i:int = 0; i < _tasks.length; i++)
			{
				if (_tasks[i].event == e.type) {
					trace(_tasks[i].task);
				ApplicationManager.performTask(_tasks[i].task);
				break;
				}
			}
			
		}
		
		override public function show():void
		{
			try {
				_mc['show']();
			} catch (e:Error) {
				trace('Cannot do function');
			}
		}
		
		override public function hide():void
		{
			try {
				_mc['hide']();
			} catch (e:Error) {
				trace('Cannot do function');
			}
		}
		
	}

}