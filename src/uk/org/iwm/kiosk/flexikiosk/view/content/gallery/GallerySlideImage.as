package uk.org.iwm.kiosk.flexikiosk.view.content.gallery 
{
	import com.afriendcalledben.style.Padding;
	import com.afriendcalledben.text.TextPanel;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import com.afriendcalledben.services.BitmapResizer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import com.afriendcalledben.display.ShapeRect;
	import flash.display.PixelSnapping;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class GallerySlideImage extends GallerySlide
	{
		private var _width:int;
		private var _height:int;
		
		private var _textBoxInterval:int = 50;
		private var _padding:Padding = new Padding(20, 20, 20, 20);
		
		private var _overlay:Sprite = new Sprite();
		public static var slideType:String = 'IMAGE';
		
		public function GallerySlideImage(w:int, h:int, image:BitmapData, txt:String = '') 
		{
			super(w, h);
			
			var nbd:BitmapData = BitmapResizer.getImageBitmapDataWithinRect(image, new Rectangle(0, 0, w, h), false);
			var bitmap:Bitmap = new Bitmap(nbd, PixelSnapping.AUTO, true);
			
			bitmap.x = -Math.ceil(bitmap.width * .5);
			bitmap.y = -Math.ceil(bitmap.height * .5);
			addChild(bitmap);
			
			_overlay.x = bitmap.x;
			_overlay.y = bitmap.y;
			addChild(_overlay);
			
			var txtFld:TextPanel = new TextPanel(txt, bitmap.width - (_padding.left + _padding.right));
			var h:int = Math.floor(txtFld.textHeight + _padding.top + _padding.bottom);
			var txtBG:ShapeRect = new ShapeRect(bitmap.width, h, 0, 0.85);
			txtBG.y = bitmap.height - h;
			_overlay.addChild(txtBG);
			
			txtFld.x = txtBG.x + _padding.left;
			txtFld.y = txtBG.y + _padding.top;
			_overlay.addChild(txtFld);
		}
		
		override public function showCaption(time:Number, delay:Number = 0):void
		{
			TweenMax.killTweensOf(_overlay);
			TweenMax.to(_overlay, time, { alpha:1, delay:delay } );
		}
		
		override public function hideCaption(time:Number, delay:Number = 0):void
		{
			TweenMax.killTweensOf(_overlay);
			TweenMax.to(_overlay, time, { alpha:0, delay:delay } );
		}
		
		override public function destroy():void
		{
			TweenMax.killTweensOf(_overlay);
			while (numChildren > 0) {
				removeChildAt(0);
			}
		}
		
		override public function get slideType():String
		{
			return 'IMAGE';
		}
	}

}