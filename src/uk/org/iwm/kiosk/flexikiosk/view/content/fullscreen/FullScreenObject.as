package uk.org.iwm.kiosk.flexikiosk.view.content.fullscreen 
{
	import com.afriendcalledben.display.ShapeRect;
	import flash.display.ShaderParameter;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.greensock.TweenMax;
	import uk.org.iwm.kiosk.flexikiosk.view.navigation.NavigationBar;
	import uk.org.iwm.kiosk.flexikiosk.model.BackgroundVO;
	import uk.org.iwm.kiosk.flexikiosk.model.NavigationBarLayoutVO;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class FullScreenObject extends Sprite
	{	
		protected var _width:int;
		protected var _height:int;
		protected var _content:Sprite = new Sprite();
		protected var _closeTask:String = '';
		
		public function FullScreenObject(w:int, h:int) 
		{
			_width = w;
			_height = h;
			
			var bg:ShapeRect = new ShapeRect(w, h, 0, 1);
			addChild(bg);
			this.alpha = 0;
			
			addChild(_content);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onCheck);
		}
		
		protected function onCheck(e:Event):void
		{
			trace('I\'m still here '+name);
		}
		
		public function show():void
		{
			TweenMax.to(this, 0.3, { alpha:1 } );
		}
		
		protected function onClose(e:Event = null):void
		{
			hide();
			if (_closeTask.length > 0) {
				ApplicationManager.performTask(_closeTask);
			}
		}
		
		protected function onAccessibility(e:Event = null):void
		{
			// Accessibility functionality goes here.
		}
		
		public function hide():void
		{
			TweenMax.to(this, 0.3, { alpha:0, onComplete:destroy } );
		}
		
		public function destroy():void
		{
			if (parent) parent.removeChild(this);
		}
		
		public function addCloseButton():void
		{
			var closeBtn:FullScreenCloseButton = new FullScreenCloseButton(_width, 150);
			closeBtn.y = _height - 150;
			addChild(closeBtn);
			TweenMax.delayedCall(1, closeBtn.addEventListener, [MouseEvent.MOUSE_DOWN, onClose]);
		}
		
		public function set closeTask(value:String):void
		{
			_closeTask = value;
		}
		
		public function get closeTask():String
		{
			return _closeTask;
		}
		
	}

}