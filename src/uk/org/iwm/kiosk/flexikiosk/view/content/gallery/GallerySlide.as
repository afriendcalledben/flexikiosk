package uk.org.iwm.kiosk.flexikiosk.view.content.gallery 
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class GallerySlide extends Sprite
	{
		private var _id:String;
		private var _showTask:String;
		private var _clickTask:String;
		
		private var _width:int;
		private var _height:int;
		
		private var _origWidth:int;
		private var _origHeight:int;
		private var _smallWidth:int;
		private var _smallHeight:int;
		private var _leftPos:int;
		private var _rightPos:int;
		
		private var _image:String = '';
		private var _audio:String = '';
		private var _video:String = '';
		private var _accessibility:String = '';
		
		public function GallerySlide(w:int, h:int) 
		{
			_width = w;
			_height = h;
		}
		
		public function showCaption(time:Number, delay:Number = 0):void
		{
			
		}
		
		public function hideCaption(time:Number, delay:Number = 0):void
		{
			
		}
		
		public function destroy():void
		{
			while (numChildren > 0) {
				removeChildAt(0);
			}
		}
		
		public function set id(value:String):void
		{
			_id = value;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function set clickTask(value:String):void
		{
			_clickTask = value;
		}
		
		public function get clickTask():String
		{
			return _clickTask;
		}
		
		public function set showTask(value:String):void
		{
			_showTask = value;
		}
		
		public function get showTask():String
		{
			return _showTask;
		}
		
		public function set origWidth(value:int):void
		{
			_origWidth = value;
		}
		
		public function get origWidth():int
		{
			return _origWidth;
		}
		
		public function set origHeight(value:int):void
		{
			_origHeight = value;
		}
		
		public function get origHeight():int
		{
			return _origHeight;
		}
		
		public function set smallWidth(value:int):void
		{
			_smallWidth = value;
		}
		
		public function get smallWidth():int
		{
			return _smallWidth;
		}
		
		public function set smallHeight(value:int):void
		{
			_smallHeight = value;
		}
		
		public function get smallHeight():int
		{
			return _smallHeight;
		}
		
		public function set leftPos(value:int):void
		{
			_leftPos = value;
		}
		
		public function get leftPos():int
		{
			return _leftPos;
		}
		
		public function set rightPos(value:int):void
		{
			_rightPos = value;
		}
		
		public function get rightPos():int
		{
			return _rightPos;
		}
		
		public function set image(value:String):void
		{
			_image = value;
		}
		
		public function get image():String
		{
			return _image;
		}
		
		public function set audio(value:String):void
		{
			_audio = value;
		}
		
		public function get audio():String
		{
			return _audio;
		}
		
		public function set video(value:String):void
		{
			_video = value;
		}
		
		public function get video():String
		{
			return _video;
		}
		
		public function set accessibility(value:String):void
		{
			_accessibility = value;
		}
		
		public function get accessibility():String
		{
			return _accessibility;
		}
		
		public function get slideType():String
		{
			return 'NONE';
		}
	}

}