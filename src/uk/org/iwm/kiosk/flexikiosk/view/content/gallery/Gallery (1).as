package uk.org.iwm.kiosk.flexikiosk.view.content.gallery 
{
	import air.update.events.DownloadErrorEvent;
	import com.afriendcalledben.display.ShapeRect;
	import com.greensock.data.TweenMaxVars;
	import com.greensock.motionPaths.RectanglePath2D;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.utils.getTimer;
	import com.greensock.TweenMax;
	import com.afriendcalledben.media.MediaManager;
	import uk.org.iwm.kiosk.flexikiosk.model.ActionVO;
	import uk.org.iwm.kiosk.flexikiosk.model.GallerySlideVO;
	import uk.org.iwm.kiosk.flexikiosk.model.GalleryVO;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.DynamicPropsPlugin;
	import uk.org.iwm.kiosk.flexikiosk.view.content.ContentBase;
	import com.afriendcalledben.text.TextManager;
	import com.afriendcalledben.keyboard.KeyCode;
	
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Gallery extends ContentBase
	{
		private var _width:int; 
		private var _height:int;
		private var _slideOverlap:int = 300;
		private var _slideDiff:int;
		private var _slidesHolder:Sprite = new Sprite();
		
		private var _prevRect:Rectangle;
		private var _currentRect:Rectangle;
		private var _nextRect:Rectangle;
		
		private var _lastDownTime:int;
		private var _lastDownPoint:Point;
		
		private var _slidePos:int = 0;
		public var _currentPos:Number = 0;
		
		private var _slideMoveTween:TweenMax;
		private var _indicatorTween:TweenMax;
		
		private var _indicator:Sprite;
		private var _indicatorInterval:int;
		
		public function Gallery(w:int, h:int) 
		{
			TweenPlugin.activate([DynamicPropsPlugin]);
			
			_width = w;
			_height = h;
			
			scrollRect = new Rectangle(0, 0, _width, _height);
			var back:ShapeRect = new ShapeRect(_width, _height, 0, 0);
			addChild(back);
			
			_prevRect = new Rectangle(0, 0, _slideOverlap, _height);
			_currentRect = new Rectangle(_slideOverlap, 0, _width - (2 * _slideOverlap), _height);
			_nextRect = new Rectangle(_width - _slideOverlap, 0, _slideOverlap, _height);
			
			//_slideDiff = (_width - _slideOverlap) - ((_width * .5) - (_slideWidth * .5));
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			trace('ADDED '+this.name+' '+getTimer());
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			trace('INIT '+this.name+' '+getTimer());
		}
		
		private function onMouseDown(e:Event):void
		{
			_lastDownTime = getTimer();
			_lastDownPoint = new Point(mouseX, mouseY);
			
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			
			if (_slideMoveTween) {
				complete();
				_slideMoveTween.kill();
			}
			
			if (_slidesHolder.numChildren > 1) startGalleryDrag();
		}
		
		private function showIndicator():void
		{
			if (!_indicator) return;
			TweenMax.killTweensOf(_indicator);
			TweenMax.to(_indicator, 0.2, { alpha: 1 } );
		}
		
		private function hideIndicator():void
		{
			if (!_indicator) return;
			TweenMax.killTweensOf(_indicator);
			TweenMax.to(_indicator, 0.2, { alpha: 0.3 } );
		}
		
		private function startGalleryDrag():void
		{
			removeEventListener(Event.ENTER_FRAME, doGalleryDrag);
			addEventListener(Event.ENTER_FRAME, doGalleryDrag);
			TweenMax.killDelayedCallsTo(hideAllCaptions);
			TweenMax.delayedCall(0.2, hideAllCaptions);
			showIndicator();
		}
		
		private function stopGalleryDrag():void
		{
			removeEventListener(Event.ENTER_FRAME, doGalleryDrag);
			var xDiff:int = _lastDownPoint.x - mouseX;
			var pos:Number = 1 / (_width * .5) * xDiff;
			_currentPos = _currentPos + pos;
			if (xDiff < -200) {
				prevSlide();
			} else if (xDiff > 200) {
				nextSlide();
			} else {
				goToSlide(_slidePos, 1);
			}
		}
		
		private function doGalleryDrag(e:Event):void
		{
			var xDiff:int = _lastDownPoint.x - mouseX;
			var pos:Number = 1 / (_width * .5) * xDiff;
			positionAllSlides(_currentPos + pos);
		}
		
		private function onMouseUp(e:Event):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			removeEventListener(Event.ENTER_FRAME, doGalleryDrag);
			TweenMax.killDelayedCallsTo(hideAllCaptions);
			
			if (getTimer() - _lastDownTime < 500 && Point.distance(new Point(mouseX, mouseY), _lastDownPoint) < 100) {
				// CLICK
				if (_prevRect.contains(mouseX, mouseY)) {
					prevSlide();
				} else if (_currentRect.contains(mouseX, mouseY)) {
					performTask();
					hideIndicator();
					goToSlide(_slidePos, 1);
				} else if (_nextRect.contains(mouseX, mouseY)) {
					nextSlide();
				}
			} else {
				// DRAG
				if (_slidesHolder.numChildren > 1) stopGalleryDrag()
			}
		}
		
		public function setContent(vo:GalleryVO):void
		{
			addChild(_slidesHolder);
			
			_slideOverlap = vo.slideOverlap;
			
			for (var i:int = 0; i < vo.getSlides().length; i++) {
				var svo:GallerySlideVO = vo.getSlides()[i];
				createSlide(svo.textId, svo.imageId, svo.audioId, svo.videoId, svo.accessibilityId, svo.clickTask, svo.showTask);
			}
			hideAllCaptions(0);
			positionAllSlides(0, true);
			var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(_slidePos));
			slide.showCaption(2.5, 2);
			if (slide.showTask.length > 0) {
				TweenMax.delayedCall(0.2, ApplicationManager.performTask, [slide.showTask, [['image', slide.image], ['video', slide.video], ['accessibility', slide.accessibility]]]);
			}
			
			if (vo.getSlides().length > 1) {
			
				_indicatorInterval = Math.round((_width - 20) / vo.getSlides().length);
				
				_indicator = new ShapeRect(_indicatorInterval, 5, 0xFFFFFF);
				_indicator.alpha = 0.3;
				_indicator.y = 10;
				_indicator.x = 10;
				addChild(_indicator);
			
			}
			//addEventListener(Event.ENTER_FRAME, onMoveMouse);
		}
		
		public function createSlide(txt:String = '', image:String = '', audio:String = '', video:String = '', accessibility:String = '', clickTask:String = '', showTask:String = ''):void
		{
			// If slide has no content, ignore.
			if (txt == '' && image == '' && audio == '' && video == '') return;
			
			var audioSnd:Sound = (audio.length > 0) ? MediaManager.getAudioByID(audio) : null;
			var imageBD:BitmapData = (image.length > 0) ? MediaManager.getBitmapByID(image) : null;
			var txt:String = (txt.length > 0) ? TextManager.getText(txt) : '';
			
			// Set width and height
			var w:int = _width - (2 * (_slideOverlap + 50));
			var h:int = _height - 100;
			
			// Determine type of slide to create - Text, Image or Audio
			var slide:GallerySlide;
			if (audio.length > 0) {
				slide = new GallerySlideAudio(w, h, audioSnd, imageBD, txt);
			} else if (video.length > 0) {
				slide = new GallerySlideVideo(w, h, imageBD, txt);
			} else if (image.length > 0) {
				slide = new GallerySlideImage(w, h, imageBD, txt);
			}
			
			slide.x = (_width * .5);
			slide.y = (_height * .5);
			slide.origWidth = slide.width;
			slide.origHeight = slide.height;
			resizeSlide(slide, _width, _height * .5);
			slide.smallWidth = slide.width;
			slide.smallHeight = slide.height;
			slide.leftPos = -(slide.smallWidth * .5) + _slideOverlap + 10;
			slide.rightPos = _width + (slide.smallWidth * .5) - _slideOverlap + 10;
			slide.image = image;
			slide.audio = audio;
			slide.video = video;
			slide.accessibility = accessibility;
			slide.clickTask = clickTask;
			slide.showTask = showTask;
			_slidesHolder.addChild(slide);
		}
		
		private function hideAllCaptions(time:Number = 0.2):void
		{
			for (var i:int = 0; i < _slidesHolder.numChildren; i++)
			{
				var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(i));
				slide.hideCaption(time);
			}
		}
		
		private function resizeSlide(slide:GallerySlide, w:Number, h:Number):void
		{
			var ratio:Number = slide.width / slide.height;
			var rW:Number = w;
			var rH:Number = h;
			if (ratio >= 1) {
				rH = w / ratio;
				if (rH > h) {
					rH = h;
					rW = rH * ratio;
				}
			} else {
				rW = h * ratio;
				if (rW > w) {
					rW = w;
					rH = rW / ratio;
				}
			}
			slide.width = rW;
			slide.height = rH;
		}
		
		override public function previousGallery():void
		{
			prevSlide();
		}
		
		override public function nextGallery():void
		{
			nextSlide();
		}
				
		public function prevSlide():void
		{
			if (_slidePos > 0) _slidePos--;
			goToSlide(_slidePos, 1);
		}
		
		public function nextSlide():void
		{
			if (_slidePos < _slidesHolder.numChildren - 1) _slidePos++;
			goToSlide(_slidePos, 1);
		}
		
		private function positionAllSlides(pos:Number, setAll:Boolean = false):void
		{
			for (var i:int = 0; i < _slidesHolder.numChildren; i++)
			{
				var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(i));
				var ipos:Number = i - pos;
				if ((ipos >= -2 && ipos <= 2) || setAll) {
					positionSlide(slide, ipos);
				}
			}
			
			if (_indicator) _indicator.x = 10 + Math.max(0, Math.min((_width - 20) - _indicatorInterval, (_width - 20) / _slidesHolder.numChildren * pos));
		}
		
		private function positionSlide(slide:GallerySlide, pos:Number, setAll:Boolean = false):void
		{
			var x:Number;
			var w:Number;
			var h:Number;
			
			var ol:Number = -(_width * .5);
			var or:Number = _width + (_width * .5);
			
			var lp:Number = slide.leftPos;
			var rp:Number = slide.rightPos;
			var cp:Number = (_width * .5);
			
			var sw:Number = slide.smallWidth;
			var sh:Number = slide.smallHeight;
			
			var ow:Number = slide.origWidth;
			var oh:Number = slide.origHeight;
			
			if (pos <= -2) {
				x = ol;
				w = sw;
				h = sh;
			} else if (pos > -2 && pos <= -1) {
				x = ol + ((lp - ol) * (pos + 2));
				w = sw;
				h = sh;
			} else if (pos > -1 && pos <= 0) {
				x = lp + ((cp - lp) * (pos + 1));
				w = sw + ((ow - sw) * (pos + 1));
				h = sh + ((oh - sh) * (pos + 1));
			} else if (pos > 0 && pos <= 1) {
				x = cp + ((rp - cp) * (pos));
				w = ow - ((ow - sw) * (pos));
				h = oh - ((oh - sh) * (pos));
			} else if (pos > 1 && pos <= 2) {
				x = rp + ((or - rp) * (pos - 1));
				w = sw;
				h = sh;
			} else if (pos > 2) {
				x = or;
				w = sw;
				h = sh;
			}
			
			slide.x = x;
			slide.width = w;
			slide.height = h;
		}
		
		public function goToSlide(id:int, time:Number = 0):void
		{
			if (_slideMoveTween) _slideMoveTween.kill();
			showIndicator();
			hideAllCaptions();
			var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(_slidePos))
			if (slide.showTask.length > 0) {
				ApplicationManager.performTask(slide.showTask, [['image', slide.image], ['video', [slide.video]], ['accessibility', slide.accessibility]]);
			}
			_slideMoveTween = TweenMax.to(this, time, { _currentPos:id, onUpdate:update, onComplete:complete } );
		}
		
		private function update():void
		{
			positionAllSlides(_currentPos);
		}
		
		private function complete():void
		{
			positionAllSlides(_currentPos, true);
			var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(_slidePos));
			slide.showCaption(2, 0.5);
			//
			hideIndicator();
		}
		
		public function performTask():void
		{
			var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(_slidePos));
			/*if (slide.slideType == 'AUDIO') {
				var aslide:GallerySlideAudio = GallerySlideAudio(_slidesHolder.getChildAt(_slidePos));
				aslide.audioClick();
			}*/
			if (slide.clickTask.length > 0) {
				ApplicationManager.performTask(slide.clickTask, [['image', slide.image], ['audio', [slide.audio]], ['video', [slide.video]], ['accessibility', slide.accessibility] ]);
			}
		}
		
		override public function hide():void
		{
			TweenMax.killDelayedCallsTo(hideAllCaptions);
			TweenMax.killDelayedCallsTo(ApplicationManager.performTask);
			for (var i:int = 0; i < _slidesHolder.numChildren; i++) {
				var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(i));
			}
		}
		
		override public function destroy():void
		{
			trace('DESTROY '+this.name+' '+getTimer());
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			if (_slideMoveTween) _slideMoveTween.kill();
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			removeEventListener(Event.ENTER_FRAME, doGalleryDrag);
			TweenMax.killDelayedCallsTo(hideAllCaptions);
			TweenMax.killDelayedCallsTo(ApplicationManager.performTask);
			while (_slidesHolder.numChildren > 0) {
				var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(0));
				slide.destroy();
				_slidesHolder.removeChildAt(0);
			}
			while (numChildren > 0) {
				removeChildAt(0);
			}
		}
		
		override public function showAccessibility():void
		{
			var slide:GallerySlide = GallerySlide(_slidesHolder.getChildAt(_slidePos));
			if (slide.accessibility.length > 0) {
				var avo:ActionVO = new ActionVO(ActionVO.SHOW_FULLSCREEN_TEXT);
				avo.addVariable('accessibility', slide.accessibility);
				ApplicationManager.performAction(avo);
			}
		}
		
	}

}