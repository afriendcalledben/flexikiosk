package uk.org.iwm.kiosk.flexikiosk.view.navigation 
{
	import com.adobe.utils.ArrayUtil;
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import uk.org.iwm.kiosk.flexikiosk.model.BackgroundVO;
	import uk.org.iwm.kiosk.flexikiosk.model.NavigationBarLayoutVO;
	import com.afriendcalledben.media.MediaManager;
	import com.afriendcalledben.style.StyleManager;
	import com.afriendcalledben.text.TextManager;
	import uk.org.iwm.kiosk.flexikiosk.model.NavigationBarButtonVO;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class NavigationBar extends Sprite
	{
		private var _bar:Sprite;
		private var _title:TextLabel;
		private var _width:int;
		private var _content:Sprite;
		private var _buttons:Sprite;
		private var _buttonTasks:Array;
		private var _positionLength:int;
		private var _alert:NavigationAlert;
		private var _layout:NavigationBarLayoutVO;
		
		public static const TITLE_LEFT:String = 'LEFT';
		public static const TITLE_CENTER:String = 'CENTER';
		public static const TITLE_RIGHT:String = 'RIGHT';
		
		public function NavigationBar(w:int, bvo:BackgroundVO = null) 
		{
			_width = w;
			
			_bar = new Sprite();
			addChild(_bar);
			
			if (!bvo) {
				bvo = new BackgroundVO(0, 0.95);
			}
			
			var bg:Shape = new Shape();
			bvo.setGraphics(bg.graphics);
			bg.graphics.drawRect(0, 0, w, 80);
			_bar.addChild(bg);
			
			_content = new Sprite();
			_bar.addChild(_content);
			
			_buttons = new Sprite();
			_content.addChild(_buttons);
			
			_positionLength = Math.floor(_width / 80) - 2;
			
			_alert = new NavigationAlert(w);
			_alert.y = 80;
			_bar.addChild(_alert);
		}
		
		public function show():void
		{
			TweenMax.to(_bar, 0.5, {y:0});
		}
		
		public function hide():void
		{
			TweenMax.to(_bar, 0.5, {y:80});
		}
		
		public function setLayout(nbvo:NavigationBarLayoutVO):void
		{
			_layout = nbvo;
			
			_bar.removeEventListener(MouseEvent.MOUSE_DOWN, onBarClick);
			
			while (_buttons.numChildren > 0) {
				var button:NavigationButton = NavigationButton(_buttons.getChildAt(0));
				button.removeEventListener(NavigationButton.CLICK, onButtonClick);
				button.removeEventListener(NavigationButton.ACCESSIBILITY, onButtonAccessibility);
				_buttons.removeChildAt(0);
			}
			
			for (var i:int = 0; i < nbvo.buttons.length; i++) {
				
				var image:BitmapData = (nbvo.buttons[i].image.length > 0) ? MediaManager.getBitmapByID(nbvo.buttons[i].image) : null;
				button = new NavigationButton(image);
				button.name = 'button'+i;
				button.enabled = nbvo.buttons[i].enabled;
				
				// POSITION
				button.x = getPosition(nbvo.buttons[i].position);
				button.addEventListener(NavigationButton.CLICK, onButtonClick);
				button.addEventListener(NavigationButton.ACCESSIBILITY, onButtonAccessibility);
				_buttons.addChild(button);
			}
			
			if (nbvo.title) {
				addTitle(nbvo.title.id, nbvo.title.position);
			} else {
				removeTitle();
			}
			
			if (nbvo.hidden) {
				hide();
			} else {
				show();
			}
			
			TweenMax.delayedCall(2, _bar.addEventListener, [MouseEvent.MOUSE_DOWN, onBarClick]);
		}
		
		private function onBarClick(e:Event):void
		{
			if (_layout.clicktask != '') {
				ApplicationManager.performTask(_layout.clicktask);
			}
		}
		
		private function onButtonClick(e:Event):void
		{
			var id:int = int(e.currentTarget.name.slice(6));
			if (_layout.buttons[id].clickTask != '') {
				ApplicationManager.performTask(_layout.buttons[id].clickTask);
			}
		}
		
		private function onButtonAccessibility(e:Event):void
		{
			var id:int = int(e.currentTarget.name.slice(6));
			if (_layout.buttons[id].accessibilityTask != '') {
				ApplicationManager.performTask(_layout.buttons[id].accessibilityTask);
			}
		}
		
		private function getPosition(position:String):int
		{
			var x:int;
			
			switch (position)
			{
				case 'LEFT' :
					x = 80;
					break;
				case 'CENTER' :
					x = (_width * .5);
					break;
				case 'RIGHT' : 
					x = 80 + (_positionLength * 80);
					break;
				default :
					x = 80 + ((Number(position) - 1) * 80);
					break;
			}
			return x;
		}
		
		public function addTitle(title:String, pos:String = TITLE_RIGHT):void
		{
			if (_title) {
				if (_content.contains(_title)) {
					_content.removeChild(_title);
				}
				_title = null;
			}
			
			_title = new TextLabel(TextManager.getText(title));
			switch (pos) {
				case TITLE_LEFT :
					_title.x = 50;
					break;
				case TITLE_CENTER :
					_title.x = (_width * .5) - (_title.width * .5);
					break;
				case TITLE_RIGHT :
					_title.x = (_width - 50) - (_title.width);
					break;
				default : 
					_title.x = getPosition(pos);
					break;
			}
			_title.y = 30;
			_content.addChildAt(_title, 0);
		}
		
		public function removeTitle():void
		{
			if (_title) {
				if (_content.contains(_title)) {
					_content.removeChild(_title);
					_title = null;
				}
			}
		}
		
		public function showAlert(alertTxt:String, hideAfterDuration:Number = NaN):void
		{
			_alert.setMessage(alertTxt);
			_alert.show();
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, onAlertClose);
			TweenMax.delayedCall(1, stage.addEventListener, [MouseEvent.MOUSE_DOWN, onAlertClose]);
		}
		
		private function onAlertClose(e:Event):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, onAlertClose);
			_alert.hide();
		}
	}

}