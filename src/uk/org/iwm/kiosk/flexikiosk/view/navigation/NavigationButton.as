package uk.org.iwm.kiosk.flexikiosk.view.navigation 
{
	import flash.geom.Rectangle;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class NavigationButton extends Sprite
	{
		private var _icon:Bitmap;
		private var _enabled:Boolean = true;
		
		public static const CLICK:String = 'NavigationButton.CLICK';
		public static const ACCESSIBILITY:String = 'NavigationButton.ACCESSIBILITY';
		
		public function NavigationButton(bd:BitmapData = null, btnRect:Rectangle = null) 
		{
			if (bd) {
				_icon = new Bitmap(bd);
				_icon.x = - (_icon.width * .5);
				_icon.y = 40 - (_icon.height * .5);
				addChild(_icon);
			}
			
			var btn:Sprite = new Sprite();
			var iconx:Number;
			var iconwidth:Number;
			with (btn.graphics) {
				beginFill(0xFF0000, 0);
				if (btnRect) {
					drawRect( btnRect.x, btnRect.y, btnRect.width, btnRect.height);
				} else if (_icon) {
					iconx = (_icon.x > -40) ? -40 : _icon.x;
					iconwidth = (_icon.width < 80) ? 80 : _icon.width;
					drawRect( iconx, 0, iconwidth, 80);
				} else {
					drawRect( -40, 0, 80, 80);
				}
				endFill();
			}
			btn.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			addChild(btn);
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		private function onDown(e:Event):void
		{
			if (_enabled) dispatchEvent(new Event(CLICK));
			TweenMax.killDelayedCallsTo(dispatchAccessbility);
			TweenMax.delayedCall(3, dispatchAccessbility);
		}
		
		private function dispatchAccessbility():void
		{
			dispatchEvent(new Event(ACCESSIBILITY));
		}
		
		private function onUp(e:Event):void
		{
			TweenMax.killDelayedCallsTo(dispatchAccessbility);
		}
		
		public function set enabled(value:Boolean):void
		{
			_enabled = value;
			if (_icon) _icon.alpha = (_enabled) ? 1 : 0.1;
		}
		
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
	}

}