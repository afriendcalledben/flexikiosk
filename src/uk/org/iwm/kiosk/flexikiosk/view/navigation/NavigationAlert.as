package uk.org.iwm.kiosk.flexikiosk.view.navigation 
{
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Sprite;
	//import uk.org.iwm.kiosk.flexikiosk.view.navigation.buttons.CloseButton;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class NavigationAlert extends Sprite
	{
		private var _content:Sprite;
		private var _message:TextLabel;
		private var _width:int;
		private var _positionLength:int;
		
		public function NavigationAlert(w:int) 
		{
			_content = new Sprite();
			addChild(_content);
			
			_width = w;
			
			var bg:ShapeRect = new ShapeRect(w, 80, 0, 0.9);
			_content.addChild(bg);
			
			_positionLength = Math.floor(_width / 80) - 2;
			
			/*var close_btn:CloseButton = new CloseButton();
			close_btn.x = 80 + (_positionLength * 80);
			_content.addChild(close_btn);*/
			
			var btn:ShapeRect = new ShapeRect(w, 80, 0, 0);
			_content.addChild(btn);
		}
		
		public function setMessage(text:String):void
		{
			if (_message) {
				if (_content.contains(_message)) {
					_content.removeChild(_message);
				}
				_message = null;
			}
			
			_message = new TextLabel(text);
			_message.x = 50;
			_message.y = 30;
			_content.addChild(_message);
		}
		
		public function show():void
		{
			TweenMax.to(_content, 0.5, { y:-80 } );
		}
		
		public function hide():void
		{
			TweenMax.to(_content, 0.5, { y:0 } );
		}
		
	}

}