package uk.org.iwm.kiosk.flexikiosk.view 
{
	import air.update.events.DownloadErrorEvent;
	import com.afriendcalledben.display.ShapeRect;
	import com.greensock.easing.Back;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import uk.org.iwm.kiosk.flexikiosk.manager.ApplicationManager;
	import com.afriendcalledben.display.Background;
	import uk.org.iwm.kiosk.flexikiosk.model.BackgroundVO;
	import uk.org.iwm.kiosk.flexikiosk.view.content.ContentBase;
	import uk.org.iwm.kiosk.flexikiosk.view.content.ExternalSWF;
	import uk.org.iwm.kiosk.flexikiosk.view.content.gallery.Gallery;
	import uk.org.iwm.kiosk.flexikiosk.view.content.contentmap.ContentMap;
	import uk.org.iwm.kiosk.flexikiosk.vo.ApplicationVO;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ApplicationLayer extends Sprite
	{
		private var _area:Rectangle;
		private var _width:int;
		private var _height:int;
		private var _background:BackgroundVO;
		private var _content:Sprite;
		private var _block:Sprite;
		private var _hidden:Boolean;
		
		public static const ANIMATION_FADE:String = 'FADE';
		public static const ANIMATION_SLIDE_TOP:String = 'SLIDE_TOP';
		public static const ANIMATION_SLIDE_BOTTOM:String = 'SLIDE_BOTTOM';
		public static const ANIMATION_SLIDE_LEFT:String = 'SLIDE_LEFT';
		public static const ANIMATION_SLIDE_RIGHT:String = 'SLIDE_RIGHT';
		
		public function ApplicationLayer(area:Rectangle, b:BackgroundVO = null) 
		{
			_area = area;
			_width = area.width;
			_height = area.height;
			
			if (b) {
				var bg:Background = new Background(_width, _height, b.color, b.alpha, b.image, b.imageRepeat);
				addChild(bg);
			}
			
			_content = new Sprite();
			addChild(_content);
			
			_block = new Sprite();
			addChild(_block);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onCheck);
		}
		
		private function onCheck(e:Event):void
		{
			trace('BLOCK ' + _block + ' ' + contains(_block));
		}
		
		public function setContent(type:String, id:String):void
		{
			while (_content.numChildren > 0) {
				var content:ContentBase = ContentBase(_content.getChildAt(0));
				content.destroy();
				_content.removeChildAt(0);
			}
			
			if (type == 'GALLERY') {
				var gallery:Gallery = new Gallery(_width, _height);
				gallery.setContent(ApplicationManager.getGalleryByID(id));
				_content.addChild(gallery);
			} else if (type == 'CONTENT_MAP') {
				var contentMap:ContentMap = new ContentMap(_width, _height);
				contentMap.setContent(ApplicationManager.getContentMapByID(id));
				_content.addChild(contentMap);
			} else if (type == 'EXTERNAL_SWF') {
				var externalSWF:ExternalSWF = new ExternalSWF(_width, _height);
				externalSWF.setContent(ApplicationManager.getExternalSWFByID(id));
				_content.addChild(externalSWF);
			}
		}
		
		public function getContent():ContentBase
		{
			var content:ContentBase;
			if (_content.numChildren > 0) {
				content = ContentBase(_content.getChildAt(0));
			}
			return content;
		}
		
		public function show(animType:String = ANIMATION_FADE):void
		{
			if (!_hidden) return;
			_hidden = false;
			while (_block.numChildren > 0) {
				_block.removeChildAt(0);
			}
			if (_content.numChildren > 0) {
				ContentBase(_content.getChildAt(0)).show();
			}
			switch (animType) {
				case ANIMATION_FADE :
					this.x = _area.x;
					this.y = _area.y;
					this.alpha = 0;
					this.visible = true;
					_content.cacheAsBitmap = true;
					TweenMax.to(this, 1, { autoAlpha:1 } );
					break;
				case ANIMATION_SLIDE_TOP :
					this.x = _area.x;
					this.y = -_area.height;
					this.alpha = 1;
					this.visible = true;
					TweenMax.to(this, 1, { y:_area.y } );
					break;
				case ANIMATION_SLIDE_BOTTOM :
					this.x = _area.x;
					this.y = ApplicationVO.applicationHeight;
					this.alpha = 1;
					this.visible = true;
					TweenMax.to(this, 1, { y:_area.y } );
					break;
				case ANIMATION_SLIDE_LEFT :
					break;
				case ANIMATION_SLIDE_RIGHT :
					break;
			}
		}
		
		public function hide(animType:String = ANIMATION_FADE):void
		{
			if (_hidden) return;
			_hidden = true;
			while (_block.numChildren > 0) {
				_block.removeChildAt(0);
			}
			var block:ShapeRect = new ShapeRect(_width, _height, 0, 0);
			_block.addChild(block);
			
			if (_content.numChildren > 0) {
				ContentBase(_content.getChildAt(0)).hide();
			}
			switch (animType) {
				case ANIMATION_FADE :
					this.x = _area.x;
					this.y = _area.y;
					this.alpha = 1;
					this.visible = true;
					_content.cacheAsBitmap = true;
					TweenMax.to(this, 1, { autoAlpha:0 } );
					break;
				case ANIMATION_SLIDE_TOP :
					this.x = _area.x;
					this.y = _area.y;
					this.alpha = 1;
					this.visible = true;
					TweenMax.to(this, 1, { y:-_area.height } );
					break;
				case ANIMATION_SLIDE_BOTTOM :
					this.x = _area.x;
					this.y = _area.y;
					this.alpha = 1;
					this.visible = true;
					TweenMax.to(this, 1, { y:ApplicationVO.applicationHeight } );
					break;
				case ANIMATION_SLIDE_LEFT :
					break;
				case ANIMATION_SLIDE_RIGHT :
					break;
			}
		}
		
		private function hideComplete():void
		{
			this.visible = false;
		}
		
		public function get hidden():Boolean
		{
			return _hidden;
		}
		
		public function set hidden(value:Boolean):void
		{
			_hidden = value;
		}
		
	}

}