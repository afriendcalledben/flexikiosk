package uk.org.iwm.kiosk.flexikiosk.overlay.ui 
{
	import flash.display.Sprite;
	import flash.display.Shape;
	import com.afriendcalledben.text.TextLabel;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import uk.msfx.utils.tracing.fp10.Tr;
	
	import flash.system.System;
	import flash.display.*;
	import flash.geom.Matrix;
	import flash.events.*;
	import flash.utils.*;
	import flash.text.*;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Monitor extends Sprite
	{
		private var _logField:TextField;
		private var _color:Number
		
		public function Monitor(w:int, h:int, col:Number = 0) 
		{
			_color = col;
			
			resize(w, h);
		}
		
		public function resize(w:int, h:int):void
		{
			while (numChildren > 0) {
				removeChildAt(0);
			}
			
			var label:TextLabel = new TextLabel('Monitor', new TextFormat('Arial', 30, 0, true));
			addChild(label);
			
			_logField = new TextField();
			_logField.width = w;
			_logField.height = h - (saveBtn.height + 10 + logLabel.textHeight + 10);
			_logField.y = logLabel.textHeight + 10;
			_logField.wordWrap = _logField.multiline = true;
			_logField.defaultTextFormat = new TextFormat('Courier', 12, 0, null, null, null, null, null, null, 20, 20, null, 10);
			addChild(_logField);
			
			_logField.htmlText = logString;
			
			var logBorder:Shape = new Shape();
			with (logBorder.graphics) {
				lineStyle(1, 0xCCCCCC);
				drawRect(0, logLabel.textHeight + 10, _logField.width, _logField.height);
			}
			addChild(logBorder);
			
			Tr.console = _logField;
		}
		
		private function added (e:Event):void {
				addEventListener(Event.ENTER_FRAME, onEnterFrame);	
				median = stage.frameRate;
				
				if(!IMAGE) {
					IMAGE = new Sprite();
					addChildAt(IMAGE, 0);
				}
					
				if(!TEXTFIELD) {
					TEXTFIELD = new TextField();
					TEXTFIELD.autoSize = TextFieldAutoSize.LEFT;
					var tf:TextFormat = new TextFormat("Arial",10,0, true);
					TEXTFIELD.x = BORDER;
					TEXTFIELD.y = BORDER;
					TEXTFIELD.defaultTextFormat =tf;
					TEXTFIELD.selectable = false;
	
					addChildAt(TEXTFIELD, 1);
				}
				intervalID = setInterval(update,1000/INTERVALTIMES);
		}

		private function kill (e:Event):void {
				removeEventListener(Event.ENTER_FRAME,onEnterFrame);
				BITMAP.dispose();
				clearInterval(intervalID);
		}
		
		private function update ():void {
				currentFPS = count*INTERVALTIMES;
				currentMemory = System.totalMemory;
				count = 0;
				createLog();
				drawData();
		}
		
		private function reset ():void {
				count = 0;
				currentFPS = 0;
				currentMemory = System.totalMemory;
				log = new Array();
				timeElapsed = 0;
				cTime = 0;
		}
		
		private function onEnterFrame (e:Event):void {
			timeElapsed = getTimer()-cTime;
			cTime = getTimer();
			count++;	
		}
		
		private function drawData ():void {
			
			if (BITMAP) BITMAP.dispose();
			
			var m:Matrix = new Matrix();
			m.translate(BORDER, 20);
			BITMAP = null;
			BITMAP = new BitmapData(WIDTH, HEIGHT, false, 0xFFFFFF);
			var dbmp:BitmapData = new BitmapData(WIDTH - BORDER * 2, HEIGHT - BORDER - 20, false, 0x0);
			var btodraw:Bitmap = new Bitmap(dbmp);
			BITMAP.draw(btodraw, m);
			
			dbmp.dispose();
			
			BITMAP.setPixel(BORDER - 1, (HEIGHT - BORDER) - getPointerHeight(stage.frameRate), 0x00FFFF);
			
			var c:int = 0;
			var i:int  = 0;
			var color:int;
			var clog:Array;
			if (log.length > (WIDTH - BORDER * 2) / 2)	i = log.length - ((WIDTH - BORDER * 2) / 2);
			
			for (i; i < log.length; i++, c++)
			{	
					clog = log[i] as Array;
					if(Number(clog[0]) > stage.frameRate*0.7){
						color = 0x007e00;
					} else if (Number(clog[0]) > stage.frameRate*0.5){
						color = 0xFFFF80;
					} else {
						color = 0xFF0000;
					}
					BITMAP.setPixel(BORDER+(c*2),(HEIGHT-BORDER)-getPointerHeight(Number(clog[0])),color);		
					BITMAP.setPixel(BORDER+(c*2)+1,(HEIGHT-BORDER)-getPointerMemoryHeight(Number(clog[1])),0x4E4E4E);	
			}

			bmptoadd = new Bitmap(BITMAP);

			if (IMAGE.numChildren > 0) IMAGE.removeChildAt(0);
			
			IMAGE.addChildAt(bmptoadd,0);
			
			TEXTFIELD.text = "FPS : "+currentFPS+"/ TE : "+timeElapsed+" ms./ MemUse : "+Math.round(currentMemory*0.00001)+" mb.";
		}
		private function getPointerHeight (n:Number):uint {
				//var maxheight = HEIGHT-BORDER-20;
				//var maxfps = stage.frameRate+10;
				return Math.round((HEIGHT-BORDER-20)*(n/(stage.frameRate+10)));
		}
		private function getPointerMemoryHeight (n:Number):uint {
				//var mem = Math.round(n*0.00001);
				//var maxheight = HEIGHT-BORDER-20;
				return Math.round((HEIGHT-BORDER-20)*((Math.round(n*0.00001))/MAX_MEMORIES));
		}
		private function createLog ():void {
			if (log.length >= Math.floor((WIDTH - BORDER * 2) * 0.5)) 		log.shift();			
			log.push([currentFPS,currentMemory]);	
		}
		
	}

}