package uk.org.iwm.kiosk.flexikiosk.overlay.ui 
{
	import flash.display.Sprite;
	import flash.display.Shape;
	import com.afriendcalledben.text.TextLabel;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogConsole;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class LogScreen extends Sprite
	{
		private var _logConsole:LogConsole;
		private var _color:Number
		
		public function LogScreen(w:int, h:int, col:Number = 0) 
		{
			_color = col;
			
			resize(w, h);
		}
		
		private function onSaveLog(e:Event):void
		{
			//Tr.saveLog();
		}
		
		private function onCopyLog(e:Event):void
		{
			//Tr.copyLogToClipboard();
		}
		
		private function onClearLog(e:Event):void
		{
			//Tr.clearLog();
		}
		
		public function resize(w:int, h:int):void
		{
			var logString:String = (_logConsole) ? _logConsole.totalLog : '';
			
			while (numChildren > 0) {
				removeChildAt(0);
			}
			
			var logLabel:TextLabel = new TextLabel('Log', new TextFormat('Arial', 30, 0, true));
			addChild(logLabel);
			
			var btnWidth:int = (w - 20) / 3;
			
			var saveBtn:TextButton = new TextButton(btnWidth, 'Save...', _color);
			saveBtn.y = h - saveBtn.height;
			saveBtn.addEventListener(MouseEvent.CLICK, onSaveLog, false, 0, true);
			addChild(saveBtn);
			
			var copyBtn:TextButton = new TextButton(btnWidth, 'Copy...', _color);
			copyBtn.x = saveBtn.width + 10;
			copyBtn.y = h - copyBtn.height;
			copyBtn.addEventListener(MouseEvent.CLICK, onCopyLog, false, 0, true);
			addChild(copyBtn);
			
			var clearBtn:TextButton = new TextButton(btnWidth, 'Clear...', _color);
			clearBtn.x = copyBtn.x + copyBtn.width + 10;
			clearBtn.y = h - clearBtn.height;
			clearBtn.addEventListener(MouseEvent.CLICK, onClearLog, false, 0, true);
			addChild(clearBtn);
			
			_logConsole = new LogConsole(w, h - (saveBtn.height + 10 + logLabel.textHeight + 10));
			_logConsole.y = logLabel.textHeight + 10;
			addChild(_logConsole);
			
			_logConsole.totalLog = logString;
			
			var logBorder:Shape = new Shape();
			with (logBorder.graphics) {
				lineStyle(1, 0xCCCCCC);
				drawRect(0, logLabel.textHeight + 10, _logConsole.width, _logConsole.height);
			}
			addChild(logBorder);
		}
		
	}

}