package uk.org.iwm.kiosk.flexikiosk.overlay.ui 
{
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class TextButton extends Sprite
	{
		
		public function TextButton(w:int, label:String, col:uint = 0xFF0000, h:Number = NaN) 
		{
			var txtL:TextLabel = new TextLabel(label, new TextFormat('Arial', 30, 0xFFFFFF, true));
			txtL.x = (w * .5) - (txtL.textWidth * .5);
			txtL.y = (isNaN(h)) ? 10 : (h * .5) - (txtL.textHeight * .5);
			
			var bg:ShapeRect = new ShapeRect(w, ((isNaN(h)) ? 20 + txtL.textHeight : h), col);
			
			addChild(bg);
			addChild(txtL);
			
			this.mouseEnabled = true;
		}
		
	}

}