package uk.org.iwm.kiosk.flexikiosk.overlay.ui 
{
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class IWMLogo extends Sprite
	{
		[Embed(source="../../../../../../../assets/img/iwm_logo.png")]
		private static var Gfx_logo:Class;
		
		public function IWMLogo(col:Number = 0) 
		{
			var bmp:Bitmap = new Gfx_logo();
			var bd:BitmapData = new BitmapData(bmp.width, bmp.height, false, toARGB(col, 1));
			bd.draw(bmp);
			addChild(new Bitmap(bd));
		}
		
		private function toARGB(rgb:uint, newAlpha:uint):uint
		{
		  var argb:uint = 0;
		  argb = (rgb);
		  argb += (newAlpha<<24);
		  return argb;
		}
		
	}

}