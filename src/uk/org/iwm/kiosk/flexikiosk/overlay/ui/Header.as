package uk.org.iwm.kiosk.flexikiosk.overlay.ui 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.afriendcalledben.text.TextLabel;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Header extends Sprite
	{
		private var _width:int;
		private var _color:Number = 0;
		
		public function Header(w:int, col:Number = 0) 
		{
			_color = col;
			
			resize(w);
		}
		
		private function onClose(e:Event):void
		{
			dispatchEvent(new Event(Event.CLOSE));
		}
		
		private function onQuit(e:Event):void
		{
			dispatchEvent(new Event('QUIT'));
		}
		
		public function resize(w:int):void
		{
			_width = w;
			
			while (numChildren > 0) {
				removeChildAt(0);
			}
			
			var logo:IWMLogo = new IWMLogo(_color);
			logo.x = logo.y = 30;
			addChild(logo);
			
			var titleLabel:TextLabel = new TextLabel('IWM FlexiKiosk', new TextFormat('Arial', 40, 0, true));
			titleLabel.x = logo.x + logo.width + 50;
			titleLabel.y = (logo.y + logo.height) - titleLabel.textHeight;
			addChild(titleLabel);
			
			var closeBtn:TextButton = new TextButton(200, 'Close', _color, logo.height);
			closeBtn.x = w - 450;
			closeBtn.y = 30;
			closeBtn.addEventListener(MouseEvent.CLICK, onClose, false, 0, true);
			addChild(closeBtn);
			
			var quitBtn:TextButton = new TextButton(200, 'Quit', _color, logo.height);
			quitBtn.x = w - 230;
			quitBtn.y = 30;
			quitBtn.addEventListener(MouseEvent.CLICK, onQuit, false, 0, true);
			addChild(quitBtn);
		}
		
	}

}