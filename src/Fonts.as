package  
{
	import flash.text.Font;
	
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 * @mxmlc -o bin/css/fonts.swf 
	 */
	public class Fonts extends Sprite
	{
		[Embed(source="../assets/fonts/IF_Std_Rg.ttf", fontName='Interface', mimeType="application/x-font", embedAsCFF="false")]
		public static var Interface:Class;
		[Embed(source="../assets/fonts/IF_Std_It.ttf", fontName='Interface', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceItalic:Class;
		[Embed(source="../assets/fonts/IF_Std_Bd.ttf", fontName='Interface', fontWeight='bold', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceBold:Class;
		[Embed(source="../assets/fonts/IF_Std_BdIt.ttf", fontName='Interface', fontWeight='bold', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceBoldItalic:Class;
		
		[Embed(source="../assets/fonts/IF_Std_Blk.ttf", fontName='InterfaceBlack', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceBlack:Class;
		[Embed(source="../assets/fonts/IF_Std_BlkIt.ttf", fontName='InterfaceBlack', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceBlackItalic:Class;
		
		[Embed(source="../assets/fonts/IF_Std_XBd.ttf", fontName='InterfaceExtraBold', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceExtraBold:Class;
		[Embed(source="../assets/fonts/IF_Std_XBdIt.ttf", fontName='InterfaceExtraBold', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceExtraBoldItalic:Class;
		
		[Embed(source="../assets/fonts/IF_Std_Lt.ttf", fontName='InterfaceThin', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceThin:Class;
		[Embed(source="../assets/fonts/IF_Std_LtIt.ttf", fontName='InterfaceThin', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceThinItalic:Class;
		
		[Embed(source="../assets/fonts/IF_Std_Lt.ttf", fontName='InterfaceLight', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceLight:Class;
		[Embed(source="../assets/fonts/IF_Std_LtIt.ttf", fontName='InterfaceLight', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var InterfaceLightItalic:Class;
		
		[Embed(source="../assets/fonts/MtNoRo.ttf", fontName='Meta', mimeType="application/x-font", embedAsCFF="false")]
		public static var Meta:Class;
		[Embed(source="../assets/fonts/MtBdRo.ttf", fontName='Meta', fontWeight='bold', mimeType="application/x-font", embedAsCFF="false")]
		public static var MetaBold:Class;
		
		[Embed(source="../assets/fonts/MtNoIt.ttf", fontName='Meta', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var MetaItalic:Class;
		[Embed(source="../assets/fonts/MtBdIt.ttf", fontName='Meta', fontWeight='bold', fontStyle='italic', mimeType="application/x-font", embedAsCFF="false")]
		public static var MetaBoldItalic:Class;
		
		[Embed(source="../assets/fonts/Arial.ttf", fontName='Arial', mimeType="application/x-font", embedAsCFF="false")]
		public static var Arial:Class;
		
		public function Fonts() 
		{
			Font.registerFont(Interface);
			Font.registerFont(InterfaceItalic);
			Font.registerFont(InterfaceBold);
			Font.registerFont(InterfaceBoldItalic);
			Font.registerFont(InterfaceExtraBold);
			Font.registerFont(InterfaceExtraBoldItalic);
			Font.registerFont(Arial);
			Font.registerFont(Meta);
			Font.registerFont(MetaBold);
			Font.registerFont(MetaItalic);
			Font.registerFont(MetaBoldItalic);
		}
		
	}

}