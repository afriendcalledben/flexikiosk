package uk.org.iwm.kiosk.iwmkioskoverlay 
{
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import uk.org.iwm.kiosk.iwmkioskoverlay.ui.Header;
	import uk.org.iwm.kiosk.iwmkioskoverlay.ui.IWMLogo;
	import uk.org.iwm.kiosk.iwmkioskoverlay.ui.LogScreen;
	import flash.desktop.NativeApplication;
	
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class IWMKioskOverlay extends Sprite
	{
		
		private var _content:Sprite;
		private var _logField:TextField;
		private var _width:int;
		private var _height:int;
		private var _isShowing:Boolean = false;
		
		private var _background:ShapeRect;
		private var _header:Header;
		private var _logScreen:LogScreen;
		
		public function IWMKioskOverlay(w:int, h:int) 
		{
			_width = w;
			_height = h;
			
			_content = new Sprite();
			addChild(_content);
			
			_background = new ShapeRect(w, h, 0xFFFFFF);
			_content.addChild(_background);
			
			_header = new Header(w, 0);
			_header.addEventListener(Event.CLOSE, onClose);
			_header.addEventListener('QUIT', onQuit);
			_content.addChild(_header);
			
			_logScreen = new LogScreen((_width * .5) - 30, _height - 230, 0);
			_logScreen.x = 30;
			_logScreen.y = 200;
			_content.addChild(_logScreen);
			
			_isShowing = true;
		}
		
		private function onClose(e:Event):void
		{
			hide();
		}
		
		private function onQuit(e:Event):void
		{
			if (NativeApplication.nativeApplication) NativeApplication.nativeApplication.exit();
		}
		
		public function show():void
		{
			_isShowing = true;
			TweenMax.to(_content, 1, { y:0 } );
		}
		
		public function hide():void
		{
			_isShowing = false;
			TweenMax.to(_content, 1, { y:-_height } );
		}
		
		public function get isShowing():Boolean
		{
			return _isShowing;
		}
		
		public function resize(w:int, h:int):void
		{
			_width = w;
			_height = h;
			
			_background.width = w;
			_background.height = h;
			
			_header.resize(w);
			_logScreen.resize(w, h);
		}
		
	}

}