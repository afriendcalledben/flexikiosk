package com.afriendcalledben.media.subtitles 
{
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.style.Padding;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class SubtitleLine extends Sprite
	{
		
		public function SubtitleLine(text:String, pad:Padding = null) 
		{
			var padding:Padding = (pad) ? pad : new Padding(14, 25, 10, 25);
			var textLabel:TextLabel = new TextLabel('<span class="subtitles">' + text + '</span>');
			textLabel.x = padding.left;
			textLabel.y = padding.top;
			var bg:ShapeRect = new ShapeRect(textLabel.textWidth + padding.left + padding.right, textLabel.textHeight + padding.top + padding.bottom, 1, 0.85);
			addChild(bg);
			addChild(textLabel);
		}
		
	}

}