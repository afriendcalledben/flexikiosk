package com.afriendcalledben.media.subtitles 
{
	import com.afriendcalledben.timeout.TimeoutManager;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import nl.inlet42.data.subtitles.SubTitleData;
	import nl.inlet42.data.subtitles.SubtitleParser;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class SubtitlesManager extends EventDispatcher
	{
		private var _subtitlesArray:Array;
		private var _loaded:Boolean = false;
		
		public function SubtitlesManager() 
		{
			
		}
		
		public function load(srtPath:String):void
		{
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onComplete);
			loader.load(new URLRequest(srtPath));
		}
		
		private function onComplete(e:Event):void
		{
			var loader:URLLoader = URLLoader(e.currentTarget);
			_subtitlesArray = SubtitleParser.parseSRT(loader.data);
			_loaded = true;
			dispatchEvent(new Event(Event.COMPLETE));
		} 
		
		public function getTitles(position:Number):String
		{
			if (!_subtitlesArray) return '';
			for (var i:int = 0; i < _subtitlesArray.length; i++) {
				var data:SubTitleData = SubTitleData(_subtitlesArray[i]);
				if (position > data.start && position < data.end) {
					return data.text;
				}
			}
			return '';
		}
		
		public function get loaded():Boolean
		{
			return _loaded;
		}
		
	}

}