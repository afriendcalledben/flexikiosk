package com.afriendcalledben.media 
{
	import com.afriendcalledben.text.TextLabel;
	import com.greensock.loading.display.ContentDisplay;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.MP3Loader;
	import com.greensock.loading.SWFLoader;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import com.greensock.loading.ImageLoader;
	import com.greensock.events.LoaderEvent;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import flash.filesystem.File;
	import flash.utils.ByteArray;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class MediaManager
	{
		[Embed(source = "../../../../assets/img/missing_image.png")]
		private static var Gfx_missing:Class;
		
		public static var BITMAPS_LOADED:String = 'bitmaps_loaded';
		public static var AUDIO_LOADED:String = 'audio_loaded';
		public static var SWFS_LOADED:String = 'swfs_loaded';
		
    	protected static var disp:EventDispatcher;
		private static var _urlDict:Dictionary = new Dictionary();
		private static var _bitmapDict:Dictionary = new Dictionary();
		private static var _videoDict:Dictionary = new Dictionary();
		private static var _subtitlesDict:Dictionary = new Dictionary();
		private static var _audioDict:Dictionary = new Dictionary();
		private static var _swfsDict:Dictionary = new Dictionary();
		private static var _bitmapList:XMLList;
		private static var _videoList:XMLList;
		private static var _subtitlesList:XMLList;
		private static var _audioList:XMLList;
		private static var _swfsList:XMLList;
		private static var _failedBitmaps:Array = new Array();
		private static var _failedAudios:Array = new Array();
		private static var _failedSwfs:Array = new Array();
		
		private static var _bitmapsLoaded:Boolean = false;
		private static var _audioLoaded:Boolean = false;
		private static var _swfsLoaded:Boolean = false;
		
		/* 
		 * *** BITMAPS ***
		 */
		
		public static function setBitmaps(xmlList:XMLList, prependDirectory:File = null, preload:Boolean = true):void
		{
			if (xmlList.length() == 0) {
				Log.log('No external images to load.');
				_bitmapsLoaded = true;
				dispatchEvent(new Event(BITMAPS_LOADED));
				return;
			}
			Log.log('Loading '+xmlList.length()+' external images...');
			_bitmapList = xmlList;
			
			var queue:LoaderMax = new LoaderMax( { onComplete:onBitmapsLoaded } );
			
			for (var i:int = 0; i < _bitmapList.length(); i++) {
				if (prependDirectory) _bitmapList[i] = prependDirectory.resolvePath(_bitmapList[i]).url;
				var id:String = _bitmapList[i].@id;
				_urlDict[id] = _bitmapList[i];
				trace(id, _urlDict[id], _bitmapList[i]);
				
				queue.append(new ImageLoader(_bitmapList[i], {name:_bitmapList[i].@id, onError:onBitmapLoadError}));
			}
			
			if (preload) queue.load();
		}
		
		private static function onBitmapsLoaded(e:LoaderEvent):void
		{
			var successTotal:int = 0;
			var failTotal:int = 0;
			for (var i:int = 0; i < _bitmapList.length(); i++) {
				// IMAGE
				var id:String = _bitmapList[i].@id;
				var bmp:Bitmap = LoaderMax.getContent(id).rawContent;
				if (bmp) {
					successTotal++;
					// Generate full-size BitmapData
					var bd:BitmapData = new BitmapData(bmp.width, bmp.height, true, 0x00000000);
					bd.draw(bmp, null, null, null, null, true);
					_bitmapDict[id] = bd;
				} else {
					failTotal++;
				}
				bmp = null;
			}
			Log.log('IMAGES LOADING COMPLETE: ' + successTotal + ' images successfully loaded.');
			if (failTotal > 0) {
				Log.log('The following '+failTotal+' images did not load. Check the xml/media.xml is correctly set: ' + _failedBitmaps.join(', '), LogEvent.ERROR);
			}
			_bitmapsLoaded = true;
			dispatchEvent(new Event(BITMAPS_LOADED));
		}
		
		private static function onBitmapLoadError(e:LoaderEvent):void
		{
			_failedBitmaps.push(ImageLoader(e.target).name);
		}
		
		/* 
		 * *** VIDEOS ***
		 */
		
		public static function setVideos(xmlList:XMLList, prependDirectory:File = null):void
		{
			_videoList = xmlList;
			
			for (var i:int = 0; i < _videoList.length(); i++) {
				if (prependDirectory) _videoList[i] = prependDirectory.resolvePath(_videoList[i]).url;
			}
		}
		
		public static function setSubtitles(xmlList:XMLList, prependDirectory:File = null):void
		{
			_subtitlesList = xmlList;
			
			for (var i:int = 0; i < _subtitlesList.length(); i++) {
				if (prependDirectory) _subtitlesList[i] = prependDirectory.resolvePath(_subtitlesList[i]).url;
			}
		}
		
		/* 
		 * *** AUDIOS ***
		 */
		
		public static function setAudios(xmlList:XMLList, prependDirectory:File = null, preload:Boolean = true):void
		{
			if (xmlList.length() == 0) {
				Log.log('No external audio to load.');
				_audioLoaded = true;
				dispatchEvent(new Event(AUDIO_LOADED));
				return;
			}
			Log.log('Loading '+xmlList.length()+' external audio files...');
			_audioList = xmlList;
			
			var queue:LoaderMax = new LoaderMax( { onComplete:onAudioLoaded } );
			
			for (var i:int = 0; i < _audioList.length(); i++) {
				if (prependDirectory) _audioList[i] = prependDirectory.resolvePath(_audioList[i]).url;
				
				queue.append(new MP3Loader(_audioList[i], {name:_audioList[i].@id, autoPlay:false, onError:onAudioLoadError}));
			}
			
			if (preload) queue.load();
		}
		
		private static function onAudioLoaded(e:LoaderEvent):void
		{
			var successTotal:int = 0;
			var failTotal:int = 0;
			for (var i:int = 0; i < _audioList.length(); i++) {
				// IMAGE
				var id:String = _audioList[i].@id;
				var snd:Sound = LoaderMax.getContent(id);
				trace(snd.bytesTotal);
				if (snd.bytesTotal > 0) {
					successTotal++;
					_audioDict[id] = snd;
				} else {
					failTotal++;
				}
			}
			Log.log('AUDIO LOADING COMPLETE: ' + successTotal + ' audio files successfully loaded.');
			if (failTotal > 0) {
				Log.log('The following '+failTotal+' audio files did not load. Check the xml/media.xml is correctly set: ' + _failedAudios.join(', '), LogEvent.ERROR);
			}
			_audioLoaded = true;
			dispatchEvent(new Event(AUDIO_LOADED));
		}
		
		private static function onAudioLoadError(e:LoaderEvent):void
		{
			_failedAudios.push(MP3Loader(e.target).name);
		}
		
		/* 
		 * *** SWFS ***
		 */
		
		public static function setSwfs(xmlList:XMLList, prependDirectory:File = null, preload:Boolean = true):void
		{
			if (xmlList.length() == 0) {
				Log.log('No external swfs to load.');
				_swfsLoaded = true;
				dispatchEvent(new Event(SWFS_LOADED));
				return;
			}
			Log.log('Loading '+xmlList.length()+' external swf files...');
			_swfsList = xmlList;
			
			 var context:LoaderContext=new LoaderContext();
			context.allowLoadBytesCodeExecution=true;
			
			var queue:LoaderMax = new LoaderMax( { onComplete:onSwfsLoaded } );
			
			for (var i:int = 0; i < _swfsList.length(); i++) {
				if (prependDirectory) _swfsList[i] = prependDirectory.resolvePath(_swfsList[i]).url;
				
				queue.append(new SWFLoader(_swfsList[i], {name:_swfsList[i].@id, autoPlay:false, context:context, onError:onSwfsLoadError}));
			}
			
			if (preload) queue.load();
		}
		
		private static function onSwfsLoaded(e:LoaderEvent):void
		{
			var successTotal:int = 0;
			var failTotal:int = 0;
			for (var i:int = 0; i < _swfsList.length(); i++) {
				// IMAGE
				var id:String = _swfsList[i].@id;
				var mc:MovieClip = MovieClip(LoaderMax.getContent(id).rawContent);
				if (mc) {
					successTotal++;
					_swfsDict[id] = mc;
				} else {
					failTotal++;
				}
			}
			Log.log('SWFS LOADING COMPLETE: ' + successTotal + ' swf files successfully loaded.');
			if (failTotal > 0) {
				Log.log('The following '+failTotal+' swf files did not load. Check the xml/media.xml is correctly set: ' + _failedSwfs.join(', '), LogEvent.ERROR);
			}
			_swfsLoaded = true;
			dispatchEvent(new Event(SWFS_LOADED));
		}
		
		private static function onSwfsLoadError(e:LoaderEvent):void
		{
			_failedSwfs.push(SWFLoader(e.target).name);
		}
		
		/*
		 * GETS
		 */
		
		public static function getBitmapByID(id:String):BitmapData
		{
			var bd:BitmapData;
			if (_bitmapDict[id]) {
				bd = _bitmapDict[id];
			} else {
				bd = getMissingImage(id);
			}
			return bd;
		}
		
		public static function getAudioByID(id:String):Sound
		{
			return _audioDict[id];
		}
		
		public static function getAudioPathByID(id:String):BitmapData
		{
			return _audioList.(attribute("id") == id)[0];
		}
		
		public static function getVideoPathByID(id:String):String
		{
			return _videoList.(attribute("id") == id)[0];
		}
		
		public static function getSubtitlesPathByID(id:String):String
		{
			return _subtitlesList.(attribute("id") == id)[0];
		}
		
		public static function getSwfByID(id:String):MovieClip
		{
			return _swfsDict[id];
		}
		
		public static function get bitmapsLoaded():Boolean
		{
			return _bitmapsLoaded;
		}
		
		public static function get audioLoaded():Boolean
		{
			return _audioLoaded;
		}
		
		public static function get swfsLoaded():Boolean
		{
			return _swfsLoaded;
		}
		
		private static function getMissingImage(id:String):BitmapData
		{
			var bmp:Bitmap = new Gfx_missing();
			
			var spr:Sprite = new Sprite();
			spr.addChild(bmp);
			
			var tf:TextFormat = new TextFormat('Arial', 12, 0xFFFFFF, true);
			var textField:TextField = new TextField();
			textField.multiline = textField.wordWrap = true;
			textField.width = bmp.width - 20;
			textField.defaultTextFormat = tf;
			textField.x = 10;
			textField.y = 62;
			
			if (_urlDict[id]) {
				textField.text = 'The image with id: \'' + id + '\' is set in the xml/media.xml but the image is missing. Image path set to: ' + String(_urlDict[id]).split('%20').join(' ');
			} else {
				textField.text = 'The image with id: \'' + id + '\' is not set in the xml/media.xml. Please add the image path.';
			}
			
			textField.autoSize = 'center';
			spr.addChild(textField);
			
			var bd:BitmapData = new BitmapData(bmp.width, bmp.height, false, 0xFF000000);
			bd.draw(spr, null, null, null, null, true);
			return bd;
		}
		
		public static function duplicateMovieClip(target:MovieClip):MovieClip
		{
			trace(target.totalFrames);
			// create duplicate
			var targetClass:Class = Object(target).constructor;
			return new targetClass();
		}
		
		public static function addEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false, p_priority:int=0, p_useWeakReference:Boolean=false):void {
			if (disp == null) { disp = new EventDispatcher(); }
			disp.addEventListener(p_type, p_listener, p_useCapture, p_priority, p_useWeakReference);
		}
		
		public static function removeEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false):void {
			if (disp == null) { return; }
			disp.removeEventListener(p_type, p_listener, p_useCapture);
		}
		
		public static function dispatchEvent(p_event:Event):void {
			if (disp == null) { return; }
			disp.dispatchEvent(p_event);
		}
		
	}

}