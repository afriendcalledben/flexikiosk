package com.afriendcalledben.media 
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import com.afriendcalledben.utils.UIDUtil;
	
	import com.greensock.TweenMax;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.SoundTransformPlugin;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class AudioManager 
	{
		private static var _volume:Number = 1;
		private static var _soundTransform:SoundTransform = new SoundTransform();
		private static var _audioDictionary:Dictionary = new Dictionary(true);
		
		public function init():void
		{
			
		}
		
		public static function playSound(snd:Sound, fade:Number = 0):void
		{
			var id:String = UIDUtil.createUID();
			var st:SoundTransform = new SoundTransform(0);
			var sc:SoundChannel = snd.play(0, 0, st);
			sc.addEventListener(Event.SOUND_COMPLETE, onSoundComplete, false, 0, true);
			_audioDictionary[id] = new AudioManagerSoundVO(snd, sc, st);
			TweenMax.to(st, fade, { volume:1 } );
		}
		
		public static function stopSound(fade:Number = 0):void
		{
			TweenMax.to(_soundTransform, fade, {volume:0, onComplete
		}
		
		private static function removeSound(id:String):void
		{
			
		}
	}

}