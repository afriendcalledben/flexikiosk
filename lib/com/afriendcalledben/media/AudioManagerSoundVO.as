package com.afriendcalledben.media 
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class AudioManagerSoundVO 
	{
		private function _sound:Sound;
		private function _soundChannel:SoundChannel;
		private function _soundTransform:SoundTransform;
		
		public function AudioManagerSoundVO(s:Sound, sc:SoundChannel, st:SoundTransform) 
		{
			_sound = s;
			_soundChannel = sc;
			_soundTransform = st;
		}
		
		public function get sound():Sound
		{
			return _sound;
		}
		
		public function set sound(value:Sound):void
		{
			_sound = value;
		}
		
		public function get soundChannel():SoundChannel
		{
			return _soundChannel;
		}
		
		public function set soundChannel(value:SoundChannel):void
		{
			_soundChannel = value;
		}
		
		public function get soundTransform():SoundTransform
		{
			return _soundTransform;
		}
		
		public function set soundTransform(value:Sound):void
		{
			_soundTransform = value;
		}
	}

}