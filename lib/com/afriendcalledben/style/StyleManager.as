package com.afriendcalledben.style
{
	import flash.events.Event;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.AntiAliasType;
	import flash.events.EventDispatcher;
	
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.CSSLoader;
	import com.greensock.events.LoaderEvent;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class StyleManager
	{
		private static var _CSS:StyleSheet;
		private static var _loaded:Boolean = false;
    	protected static var disp:EventDispatcher;
		
		public static function setText(tf:TextField, txt:String, auto:String = ''):void
		{
			tf.styleSheet = _CSS;
			tf.embedFonts = true;
			tf.selectable = false;
			tf.antiAliasType = AntiAliasType.ADVANCED;
			tf.htmlText = txt;
			switch (auto)
			{
				case 'left' :
				tf.autoSize = TextFieldAutoSize.LEFT;
				break;
				case 'right' :
				tf.autoSize = TextFieldAutoSize.RIGHT;
				break;
				case 'center' :
				tf.autoSize = TextFieldAutoSize.CENTER;
				break;
			}
		}
		
		public static function loadStyleSheet(path:String):void
		{
			var loader:CSSLoader = new CSSLoader(path, { name:'css', noCache:true, autoDispose:true, onComplete:onCSSLoaded } );
			loader.load();
		}
		
		private static function onCSSLoaded(e:LoaderEvent):void
		{
			_CSS = LoaderMax.getContent('css');
			_CSS.setStyle(".iwm_kiosk_default", { color:"#FFFFFF", fontFamily:"Arial", fontSize: "20px" });
			_loaded = true;
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public static function get loaded():Boolean
		{
			return _loaded;
		}
		
		public static function addEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false, p_priority:int=0, p_useWeakReference:Boolean=false):void {
			if (disp == null) { disp = new EventDispatcher(); }
			disp.addEventListener(p_type, p_listener, p_useCapture, p_priority, p_useWeakReference);
		}
		
		public static function removeEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false):void {
			if (disp == null) { return; }
			disp.removeEventListener(p_type, p_listener, p_useCapture);
		}
		
		public static function dispatchEvent(p_event:Event):void {
			if (disp == null) { return; }
			disp.dispatchEvent(p_event);
		}
		
	}

}