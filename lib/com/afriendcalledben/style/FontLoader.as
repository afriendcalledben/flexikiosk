package com.afriendcalledben.style
{
	import flash.events.EventDispatcher;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.system.LoaderContext;
	import flash.net.URLLoaderDataFormat;
	import flash.events.Event;
	import flash.events.ErrorEvent;
	import flash.errors.*;
	
	public class FontLoader extends EventDispatcher
	{
		
		private var _loadCount:int;
		private var _loaded:Boolean = false;
		
		public function FontLoader()
		{
			
		}
		
		/**
		 * There's no attempt to deal with duplication so if this is an issue then it should be handled before calling this function
		 */
		public function load(swfs:Array):void
		{
			_loadCount = swfs.length;
			
			for (var i:int = 0; i < _loadCount; i++)
			{
				var fontLoader:URLLoader = new URLLoader();
				fontLoader.dataFormat = URLLoaderDataFormat.BINARY;
				fontLoader.addEventListener(Event.COMPLETE, handleURLComplete);
				try {
					fontLoader.load(new URLRequest(swfs[i]));
				}
				catch(argErr:ArgumentError)
				{
					trace("Bad headerz");
				}
				catch(memErr:MemoryError)
				{
					trace("UTF-8 parsing error... or just not enough RAM for your POST data.");
				}
				catch(securityErr:SecurityError)
				{
					trace("Either you're local-networking, or hitting a no-no port.");
				}
				catch(typeErr:TypeError)
				{
					trace("t3h URL is t3h null");
				}
			}
		}
		
		private function handleURLComplete(e:Event):void
		{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.INIT, handleInit);
			var context:LoaderContext = new LoaderContext();
			context.allowCodeImport = true;
			loader.loadBytes(e.target.data, context);
		}
		
		private function handleInit(e:Event):void
		{
			_loadCount--;
			if (_loadCount <= 0) {
				_loaded = true;
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		public function get loaded():Boolean
		{
			return _loaded;
		}
	}
}