package com.afriendcalledben.display 
{
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.display.GradientType;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class FadeBox extends Sprite
	{
		
		public function FadeBox(w:int, h:int, d:int = 0, col:Number = 0xFF0000) 
		{
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(w, h, d * Math.PI / 180);
			
			graphics.beginGradientFill(GradientType.LINEAR, [col, col], [1, 0], [0, 255], matrix);
			graphics.drawRect(0, 0, w, h);
			graphics.endFill();
		}
		
	}

}