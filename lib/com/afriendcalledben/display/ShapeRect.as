package com.afriendcalledben.display 
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class ShapeRect extends Sprite
	{
		
		public function ShapeRect(w:int, h:int, col:uint = 0xFF0000, alpha:Number = 1) 
		{
			with (graphics) {
				beginFill(col, alpha);
				drawRect(0, 0, w, h);
				endFill();
			}
		}
		
	}

}