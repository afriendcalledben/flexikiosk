package com.afriendcalledben.text 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import com.afriendcalledben.style.StyleManager;
	import flash.geom.ColorTransform;
	import flash.text.TextFormat;
	import flash.text.TextLineMetrics;
	import flash.utils.getTimer;
	import com.afriendcalledben.display.ShapeRect;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class TextLabel extends Sprite
	{
		private var _textTF:TextField;
		private var _textWidth:int;
		private var _textHeight:int;
		private var _baseLine:int;
		
		public function TextLabel(label:String, tf:TextFormat = null, bg:Number = NaN) 
		{
			_textTF = new TextField();
			StyleManager.setText(_textTF, label, 'left');
			if (tf) {
				_textTF.embedFonts = false;
				_textTF.styleSheet = null;
				_textTF.setTextFormat(tf);
			}
			addChild(_textTF);
			
			positionText(_textTF);
			
			if (!isNaN(bg)) {
				var bgr:ShapeRect = new ShapeRect(_textWidth + 6, _textHeight + 6, bg, 1);
				bgr.x = bgr.y = -3;
				addChildAt(bgr, 0);
			}
		}
		
		private function positionText(tf:TextField):void
		{
			var tlm:TextLineMetrics = tf.getLineMetrics(0);
			
			var bd:BitmapData = new BitmapData(tf.width, tf.height, true, 0x00000000);
			bd.draw(tf);
			
			// Find left of text
			
			var vector:String = new Vector.<uint>(tf.height, true).join(',');
			var textLeft:int = 0;
			
			for (var i:int = 0; i < tf.width; i++) {
				var vector2:String = bd.getVector(new Rectangle(i, 0, 1, tf.height)).join(',');
				if (vector != vector2) {
					textLeft = i;
					break;
				}
			}
			
			// Find beginning of text from top
			
			vector = new Vector.<uint>(tf.width, true).join(',');
			var textTop:int = 0;
			
			for (i = 0; i < tf.height; i++) {
				vector2 = bd.getVector(new Rectangle(0, i, tf.width, 1)).join(',');
				if (vector != vector2) {
					textTop = i;
					break;
				}
			}
			
			tf.x = -textLeft;
			tf.y = -textTop;
			_textHeight = tlm.height - textTop + 2;
			_textWidth = tlm.width;
			_baseLine = tlm.ascent - textTop + 2;
		}
		
		public function get textWidth():Number
		{
			return _textWidth;
		}
		
		public function get textHeight():Number
		{
			return _textHeight;
		}
		
		public function get baseLine():Number
		{
			return _baseLine;
		}
		
		public function get text():String
		{
			return _textTF.text;
		}
		
		public function set text(value:String):void
		{
			_textTF.text = value;
			positionText(_textTF);
		}
		
	}

}