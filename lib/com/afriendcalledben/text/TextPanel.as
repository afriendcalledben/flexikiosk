package com.afriendcalledben.text 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import com.afriendcalledben.style.StyleManager;
	import flash.geom.ColorTransform;
	import flash.text.TextLineMetrics;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class TextPanel extends Sprite
	{
		private var _textWidth:int;
		private var _textHeight:int;
		private var _baseLine:int;
		
		public function TextPanel(body:String, width:Number = 100) 
		{
			var tf:TextField = new TextField();
			tf.width = width;
			tf.embedFonts = tf.multiline = tf.wordWrap = true;
			StyleManager.setText(tf, body, 'center');
			addChild(tf);
			
			positionText(tf);
		}
		
		private function positionText(tf:TextField):void
		{
			var tlm:TextLineMetrics = tf.getLineMetrics(0);
			var tlmLastLine:TextLineMetrics = tf.getLineMetrics(tf.numLines - 1);
			
			var bd:BitmapData = new BitmapData(tf.width, tf.height, true, 0x00000000);
			bd.draw(tf);
			
			// Find left of text
			
			var vector:String = new Vector.<uint>(tf.height, true).join(',');
			var textLeft:int = 0;
			
			for (var i:int = 0; i < tf.width; i++) {
				var vector2:String = bd.getVector(new Rectangle(i, 0, 1, tf.height)).join(',');
				if (vector != vector2) {
					textLeft = i;
					break;
				}
			}
			
			// Find beginning of text from top
			
			vector = new Vector.<uint>(tf.width, true).join(',');
			var textTop:int = 0;
			
			for (i = 0; i < tlm.height; i++) {
				vector2 = bd.getVector(new Rectangle(0, i, tf.width, 1)).join(',');
				if (vector != vector2) {
					textTop = i;
					break;
				}
			}
			
			tf.x = -textLeft;
			tf.y = -textTop;
			_textHeight = tf.height - textTop + 2;
			_textWidth = tf.width;
		}
		
		public function get textWidth():Number
		{
			return _textWidth;
		}
		
		public function get textHeight():Number
		{
			return _textHeight;
		}
		
		
	}

}