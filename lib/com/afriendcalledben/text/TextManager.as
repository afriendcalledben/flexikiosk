package com.afriendcalledben.text {
	
	public class TextManager
	{
		
		private static var _xml				: XML;

		public function TextManager() { trace("TextManager : do not instantiate"); }		
		
		public static function setXML(xml:XML):void
		{
			_xml = xml;
		}
		
		public static function getText(id:String) : String
		{
			if (_xml..text.(@id == id).length() > 0) {
				return '<span class="iwm_kiosk_default">'+processHTMLText(_xml..text.(@id == id)[0])+'</span>';
			} else {
				return '<span class="iwm_kiosk_default">!!! TEXT NOT FOUND   > Text ID:\"'+id+'\" Check the text has been set in xml/text.xml !!!</span>';
			}
		}
		
		public static function getReadTime(label:String) : Number
		{
			if (_xml..text.(@label == label).length() > 0) {
				return Number(_xml..text.(@label == label)[0].attribute('readtime'));
			} else {
				return NaN;
			}
		}
		
		public static function processHTMLText(str:String):String
		{
			str = str.split("\r").join('');
			str = str.split("\n").join('');
			str = str.split("&ndash;").join('–');
			str = str.split("&mdash;").join('—');
			str = str.split("&iexcl;").join('¡');
			str = str.split("&iquest;").join('¿');
			str = str.split("&quot;").join('"');
			str = str.split("&ldquo;").join('“');
			str = str.split("&rdquo;").join('”');
			str = str.split("&#39;").join('\'');
			str = str.split("&lsquo;").join('‘');
			str = str.split("&rsquo;").join('’');
			str = str.split("&laquo;").join('«');
			str = str.split("&raquo;").join('»');
			str = str.split("&amp;").join('&');
			str = str.split("&cent;").join('¢');
			str = str.split("&copy;").join('©');
			str = str.split("&divide;").join('÷');
			str = str.split("&gt;").join('>');
			str = str.split("&lt;").join('<');
			str = str.split("&micro;").join('µ');
			str = str.split("&middot;").join('·');
			str = str.split("&euro;").join('€');
			str = str.split("&pound;").join('£');
			str = str.split("&reg;").join('®');
			str = str.split("&trade;").join('™');
			str = str.split("&yen;").join('¥');
			str = str.split("&deg;").join('°');
			str = str.split("</p>").join('</p><br />');
			return str;
		}
	}
}