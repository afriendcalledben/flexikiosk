/**
* Flash AS3 Video event
* @author Ben Tandy
* @version 1.0
*/
package com.afriendcalledben.events {
	
    import com.afriendcalledben.events.CustomEvent;

    public class VideoEvent extends CustomEvent
	{
		
		public static const BUFFER_EMPTY:String = "buffer_empty";
		public static const BUFFER_FULL:String = "buffer_full";
		public static const BUFFER_FLUSH:String = "buffer_flush";
		public static const VIDEO_STARTED:String = "video_started";
		public static const VIDEO_CUED:String = "video_cued";
		public static const VIDEO_STOPPED:String = "video_stopped";
		public static const VIDEO_PAUSED:String = "video_paused";
		public static const VIDEO_UNPAUSED:String = "video_unpaused";
		public static const VIDEO_NOT_FOUND:String = "video_not_found";
		public static const META_RECIEVED:String = "meta_recieved";
		
		/**
		 * Creates an instance of a VideoEvent.
		 * @param type		The type of event.
		 * @param obj		Additional data for the event.
		 */
		public function VideoEvent(type:String, obj:Object = null) {
			
			super(type, obj);
		
		}
	
	}
	
}