package com.afriendcalledben.services 
{
	import flash.display.BitmapData;
	import com.adobe.images.JPGEncoder;
	import flash.utils.ByteArray;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	import uk.org.iwm.socialinterp.model.ConfigVO;
	import com.afriendcalledben.utils.UIDUtil;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class TempFileStore 
	{
		private static var _objectDirectory:File;
		
		public static function storeBitmap(bd:BitmapData, name:String = ''):void
		{
			if (name == '') return;
			if (!_objectDirectory) createObjectDirectory();
			var jpgEncoder:JPGEncoder = new JPGEncoder(75);
			var ba:ByteArray = jpgEncoder.encode(bd);
			var image:File = _objectDirectory.resolvePath(name);
			var fs:FileStream = new FileStream();
			try
			{
				//open file in write mode
				fs.open(image, FileMode.WRITE);
				//write bytes from the byte array
				fs.writeBytes(ba);
				//close the file
				fs.close();
			}
			catch (e:Error)
			{
				trace(e.message);
			}
		}
		
		public static function storeText(str:String, name:String = ''):void
		{
			if (!_objectDirectory) createObjectDirectory();
			var text:File = _objectDirectory.resolvePath(name);
			var fs:FileStream = new FileStream();
			try
			{
				//open file in write mode
				fs.open(text, FileMode.WRITE);
				//write bytes from the byte array
				fs.writeUTF(str);
				//close the file
				fs.close();
			}
			catch (e:Error)
			{
				trace(e.message);
			}
		}
		
		private static function createTempDirectory():void
		{
			_objectDirectory = File.createTempDirectory();
		}
		
	}

}