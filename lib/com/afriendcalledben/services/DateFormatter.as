package com.afriendcalledben.services 
{
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class DateFormatter 
	{
		
		public static function getDayString(thedate:Date, short:Boolean = false):String
		{
			var days:Array = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			var daysShort:Array = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
			
			return (short) ? daysShort[thedate.day] : days[thedate.day];
		}
		
		public static function getDateString(thedate:Date, delimiter:String = "/"):String
		{
			var date:String = (thedate.date < 10) ? '0' + thedate.date : '' + thedate.date;
			var month:String = (thedate.month + 1 < 10) ? '0' + (thedate.month + 1) : '' + (thedate.month + 1);
			var year:String = String(thedate.fullYear);
			
			return date + delimiter + month + delimiter + year;
		}
		
		public static function getTimeString(thedate:Date, delimiter:String = ":"):String
		{
			var hour:String = (thedate.hours < 10) ? '0' + thedate.hours : '' + thedate.hours;
			var minute:String = (thedate.minutes < 10) ? '0' + thedate.minutes : '' + thedate.minutes;
			
			return hour + delimiter + minute;
		}
		
	}

}